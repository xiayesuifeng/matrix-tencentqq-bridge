# Matrix-tencentqq-bridge

Matrix-tencentqq-bridge 是 Matrix 和 Tencent QQ 之间的桥接机器人

基于 [mautrix-go](https://github.com/matrix-org/mautrix-go) 和 [MiraiGo](https://github.com/Mrs4s/MiraiGo) 实现

项目实现参考了 [mautrix-whatsapp](https://github.com/mautrix/whatsapp)

## 功能实现

- [x] 帐号密码登录
- [x] 二维码登录
- [ ] 退出登录
- [ ] 双重傀儡(double puppet)
- [ ] QQ 消息同步到 Matrix (仅完成了群聊)
- [ ] Matrix 消息同步到 QQ (仅完成了群聊文本与图片)
- [ ] 群聊历史聊天记录同步

## 支持的消息类型
- [x] 文本
- [x] 图片
- [ ] 语音
- [ ] 表情
- [x] At
- [x] 回复
- [ ] 长消息(仅群聊/私聊)
- [ ] 链接分享
- [ ] 小程序
- [ ] 短视频
- [ ] 合并转发
- [x] 群文件(上传与接收信息)

## 数据库支持
* PostgreSQL
* SQLite3