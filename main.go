package main

import (
	"flag"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/bridge"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/config"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
	"os"
	"os/signal"
	"syscall"
)

var (
	configPath           = flag.String("config", "etc/config.yaml", "The path to your config file.")
	generateRegistration = flag.Bool("generate-registration", false, "Generate registration and quit.")
	registrationPath     = flag.String("registration", "etc/registration.yaml", "The path where to save the appservice registration.")
	help                 = flag.Bool("help", false, "show usage help")

	conf           *config.Config
	bridgeInstance *bridge.Bridge

	log *zap.Logger
)

func init() {
	flag.Parse()
	if *help {
		flag.Usage()
		os.Exit(0)
	}

	log = zap.NewExample()

	bytes, err := os.ReadFile(*configPath)
	if err != nil {
		log.Fatal("read config file", zap.Error(err))
	}

	conf, err = config.Parse(bytes)
	if err != nil {
		log.Fatal("parse config", zap.Error(err))
	}

	if *generateRegistration {
		registration, err := conf.NewRegistration()
		if err != nil {
			log.Fatal("parse config", zap.Error(err))
		}

		bytes, err := yaml.Marshal(registration)
		if err != nil {
			log.Fatal("marshal registration config", zap.Error(err))
		}

		if err := os.WriteFile(*registrationPath, bytes, 0644); err != nil {
			log.Fatal("save registration yaml file", zap.Error(err))
		}

		if err := conf.Save(*configPath); err != nil {
			log.Fatal("update config file app service token info", zap.Error(err))
		}

		os.Exit(0)
	}

	bridgeInstance, err = bridge.NewBridge(conf, log.Named("bridge"))
	if err != nil {
		log.Fatal("init bridge instance", zap.Error(err))
	}
}

func main() {
	bridgeInstance.Start()

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

	bridgeInstance.Stop()
}
