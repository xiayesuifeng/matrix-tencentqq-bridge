// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/migrate"

	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/message"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/portal"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/puppet"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/user"

	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql"
)

// Client is the client that holds all ent builders.
type Client struct {
	config
	// Schema is the client for creating, migrating and dropping schema.
	Schema *migrate.Schema
	// Message is the client for interacting with the Message builders.
	Message *MessageClient
	// Portal is the client for interacting with the Portal builders.
	Portal *PortalClient
	// Puppet is the client for interacting with the Puppet builders.
	Puppet *PuppetClient
	// User is the client for interacting with the User builders.
	User *UserClient
}

// NewClient creates a new client configured with the given options.
func NewClient(opts ...Option) *Client {
	cfg := config{log: log.Println, hooks: &hooks{}}
	cfg.options(opts...)
	client := &Client{config: cfg}
	client.init()
	return client
}

func (c *Client) init() {
	c.Schema = migrate.NewSchema(c.driver)
	c.Message = NewMessageClient(c.config)
	c.Portal = NewPortalClient(c.config)
	c.Puppet = NewPuppetClient(c.config)
	c.User = NewUserClient(c.config)
}

// Open opens a database/sql.DB specified by the driver name and
// the data source name, and returns a new client attached to it.
// Optional parameters can be added for configuring the client.
func Open(driverName, dataSourceName string, options ...Option) (*Client, error) {
	switch driverName {
	case dialect.MySQL, dialect.Postgres, dialect.SQLite:
		drv, err := sql.Open(driverName, dataSourceName)
		if err != nil {
			return nil, err
		}
		return NewClient(append(options, Driver(drv))...), nil
	default:
		return nil, fmt.Errorf("unsupported driver: %q", driverName)
	}
}

// Tx returns a new transactional client. The provided context
// is used until the transaction is committed or rolled back.
func (c *Client) Tx(ctx context.Context) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, fmt.Errorf("ent: cannot start a transaction within a transaction")
	}
	tx, err := newTx(ctx, c.driver)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = tx
	return &Tx{
		ctx:     ctx,
		config:  cfg,
		Message: NewMessageClient(cfg),
		Portal:  NewPortalClient(cfg),
		Puppet:  NewPuppetClient(cfg),
		User:    NewUserClient(cfg),
	}, nil
}

// BeginTx returns a transactional client with specified options.
func (c *Client) BeginTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {
	if _, ok := c.driver.(*txDriver); ok {
		return nil, fmt.Errorf("ent: cannot start a transaction within a transaction")
	}
	tx, err := c.driver.(interface {
		BeginTx(context.Context, *sql.TxOptions) (dialect.Tx, error)
	}).BeginTx(ctx, opts)
	if err != nil {
		return nil, fmt.Errorf("ent: starting a transaction: %w", err)
	}
	cfg := c.config
	cfg.driver = &txDriver{tx: tx, drv: c.driver}
	return &Tx{
		ctx:     ctx,
		config:  cfg,
		Message: NewMessageClient(cfg),
		Portal:  NewPortalClient(cfg),
		Puppet:  NewPuppetClient(cfg),
		User:    NewUserClient(cfg),
	}, nil
}

// Debug returns a new debug-client. It's used to get verbose logging on specific operations.
//
//	client.Debug().
//		Message.
//		Query().
//		Count(ctx)
//
func (c *Client) Debug() *Client {
	if c.debug {
		return c
	}
	cfg := c.config
	cfg.driver = dialect.Debug(c.driver, c.log)
	client := &Client{config: cfg}
	client.init()
	return client
}

// Close closes the database connection and prevents new queries from starting.
func (c *Client) Close() error {
	return c.driver.Close()
}

// Use adds the mutation hooks to all the entity clients.
// In order to add hooks to a specific client, call: `client.Node.Use(...)`.
func (c *Client) Use(hooks ...Hook) {
	c.Message.Use(hooks...)
	c.Portal.Use(hooks...)
	c.Puppet.Use(hooks...)
	c.User.Use(hooks...)
}

// MessageClient is a client for the Message schema.
type MessageClient struct {
	config
}

// NewMessageClient returns a client for the Message from the given config.
func NewMessageClient(c config) *MessageClient {
	return &MessageClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `message.Hooks(f(g(h())))`.
func (c *MessageClient) Use(hooks ...Hook) {
	c.hooks.Message = append(c.hooks.Message, hooks...)
}

// Create returns a create builder for Message.
func (c *MessageClient) Create() *MessageCreate {
	mutation := newMessageMutation(c.config, OpCreate)
	return &MessageCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Message entities.
func (c *MessageClient) CreateBulk(builders ...*MessageCreate) *MessageCreateBulk {
	return &MessageCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Message.
func (c *MessageClient) Update() *MessageUpdate {
	mutation := newMessageMutation(c.config, OpUpdate)
	return &MessageUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *MessageClient) UpdateOne(m *Message) *MessageUpdateOne {
	mutation := newMessageMutation(c.config, OpUpdateOne, withMessage(m))
	return &MessageUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *MessageClient) UpdateOneID(id int) *MessageUpdateOne {
	mutation := newMessageMutation(c.config, OpUpdateOne, withMessageID(id))
	return &MessageUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Message.
func (c *MessageClient) Delete() *MessageDelete {
	mutation := newMessageMutation(c.config, OpDelete)
	return &MessageDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a delete builder for the given entity.
func (c *MessageClient) DeleteOne(m *Message) *MessageDeleteOne {
	return c.DeleteOneID(m.ID)
}

// DeleteOneID returns a delete builder for the given id.
func (c *MessageClient) DeleteOneID(id int) *MessageDeleteOne {
	builder := c.Delete().Where(message.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &MessageDeleteOne{builder}
}

// Query returns a query builder for Message.
func (c *MessageClient) Query() *MessageQuery {
	return &MessageQuery{
		config: c.config,
	}
}

// Get returns a Message entity by its id.
func (c *MessageClient) Get(ctx context.Context, id int) (*Message, error) {
	return c.Query().Where(message.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *MessageClient) GetX(ctx context.Context, id int) *Message {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// Hooks returns the client hooks.
func (c *MessageClient) Hooks() []Hook {
	return c.hooks.Message
}

// PortalClient is a client for the Portal schema.
type PortalClient struct {
	config
}

// NewPortalClient returns a client for the Portal from the given config.
func NewPortalClient(c config) *PortalClient {
	return &PortalClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `portal.Hooks(f(g(h())))`.
func (c *PortalClient) Use(hooks ...Hook) {
	c.hooks.Portal = append(c.hooks.Portal, hooks...)
}

// Create returns a create builder for Portal.
func (c *PortalClient) Create() *PortalCreate {
	mutation := newPortalMutation(c.config, OpCreate)
	return &PortalCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Portal entities.
func (c *PortalClient) CreateBulk(builders ...*PortalCreate) *PortalCreateBulk {
	return &PortalCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Portal.
func (c *PortalClient) Update() *PortalUpdate {
	mutation := newPortalMutation(c.config, OpUpdate)
	return &PortalUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *PortalClient) UpdateOne(po *Portal) *PortalUpdateOne {
	mutation := newPortalMutation(c.config, OpUpdateOne, withPortal(po))
	return &PortalUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *PortalClient) UpdateOneID(id int) *PortalUpdateOne {
	mutation := newPortalMutation(c.config, OpUpdateOne, withPortalID(id))
	return &PortalUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Portal.
func (c *PortalClient) Delete() *PortalDelete {
	mutation := newPortalMutation(c.config, OpDelete)
	return &PortalDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a delete builder for the given entity.
func (c *PortalClient) DeleteOne(po *Portal) *PortalDeleteOne {
	return c.DeleteOneID(po.ID)
}

// DeleteOneID returns a delete builder for the given id.
func (c *PortalClient) DeleteOneID(id int) *PortalDeleteOne {
	builder := c.Delete().Where(portal.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &PortalDeleteOne{builder}
}

// Query returns a query builder for Portal.
func (c *PortalClient) Query() *PortalQuery {
	return &PortalQuery{
		config: c.config,
	}
}

// Get returns a Portal entity by its id.
func (c *PortalClient) Get(ctx context.Context, id int) (*Portal, error) {
	return c.Query().Where(portal.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *PortalClient) GetX(ctx context.Context, id int) *Portal {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// Hooks returns the client hooks.
func (c *PortalClient) Hooks() []Hook {
	return c.hooks.Portal
}

// PuppetClient is a client for the Puppet schema.
type PuppetClient struct {
	config
}

// NewPuppetClient returns a client for the Puppet from the given config.
func NewPuppetClient(c config) *PuppetClient {
	return &PuppetClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `puppet.Hooks(f(g(h())))`.
func (c *PuppetClient) Use(hooks ...Hook) {
	c.hooks.Puppet = append(c.hooks.Puppet, hooks...)
}

// Create returns a create builder for Puppet.
func (c *PuppetClient) Create() *PuppetCreate {
	mutation := newPuppetMutation(c.config, OpCreate)
	return &PuppetCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of Puppet entities.
func (c *PuppetClient) CreateBulk(builders ...*PuppetCreate) *PuppetCreateBulk {
	return &PuppetCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for Puppet.
func (c *PuppetClient) Update() *PuppetUpdate {
	mutation := newPuppetMutation(c.config, OpUpdate)
	return &PuppetUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *PuppetClient) UpdateOne(pu *Puppet) *PuppetUpdateOne {
	mutation := newPuppetMutation(c.config, OpUpdateOne, withPuppet(pu))
	return &PuppetUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *PuppetClient) UpdateOneID(id int) *PuppetUpdateOne {
	mutation := newPuppetMutation(c.config, OpUpdateOne, withPuppetID(id))
	return &PuppetUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for Puppet.
func (c *PuppetClient) Delete() *PuppetDelete {
	mutation := newPuppetMutation(c.config, OpDelete)
	return &PuppetDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a delete builder for the given entity.
func (c *PuppetClient) DeleteOne(pu *Puppet) *PuppetDeleteOne {
	return c.DeleteOneID(pu.ID)
}

// DeleteOneID returns a delete builder for the given id.
func (c *PuppetClient) DeleteOneID(id int) *PuppetDeleteOne {
	builder := c.Delete().Where(puppet.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &PuppetDeleteOne{builder}
}

// Query returns a query builder for Puppet.
func (c *PuppetClient) Query() *PuppetQuery {
	return &PuppetQuery{
		config: c.config,
	}
}

// Get returns a Puppet entity by its id.
func (c *PuppetClient) Get(ctx context.Context, id int) (*Puppet, error) {
	return c.Query().Where(puppet.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *PuppetClient) GetX(ctx context.Context, id int) *Puppet {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// Hooks returns the client hooks.
func (c *PuppetClient) Hooks() []Hook {
	return c.hooks.Puppet
}

// UserClient is a client for the User schema.
type UserClient struct {
	config
}

// NewUserClient returns a client for the User from the given config.
func NewUserClient(c config) *UserClient {
	return &UserClient{config: c}
}

// Use adds a list of mutation hooks to the hooks stack.
// A call to `Use(f, g, h)` equals to `user.Hooks(f(g(h())))`.
func (c *UserClient) Use(hooks ...Hook) {
	c.hooks.User = append(c.hooks.User, hooks...)
}

// Create returns a create builder for User.
func (c *UserClient) Create() *UserCreate {
	mutation := newUserMutation(c.config, OpCreate)
	return &UserCreate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// CreateBulk returns a builder for creating a bulk of User entities.
func (c *UserClient) CreateBulk(builders ...*UserCreate) *UserCreateBulk {
	return &UserCreateBulk{config: c.config, builders: builders}
}

// Update returns an update builder for User.
func (c *UserClient) Update() *UserUpdate {
	mutation := newUserMutation(c.config, OpUpdate)
	return &UserUpdate{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOne returns an update builder for the given entity.
func (c *UserClient) UpdateOne(u *User) *UserUpdateOne {
	mutation := newUserMutation(c.config, OpUpdateOne, withUser(u))
	return &UserUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// UpdateOneID returns an update builder for the given id.
func (c *UserClient) UpdateOneID(id int) *UserUpdateOne {
	mutation := newUserMutation(c.config, OpUpdateOne, withUserID(id))
	return &UserUpdateOne{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// Delete returns a delete builder for User.
func (c *UserClient) Delete() *UserDelete {
	mutation := newUserMutation(c.config, OpDelete)
	return &UserDelete{config: c.config, hooks: c.Hooks(), mutation: mutation}
}

// DeleteOne returns a delete builder for the given entity.
func (c *UserClient) DeleteOne(u *User) *UserDeleteOne {
	return c.DeleteOneID(u.ID)
}

// DeleteOneID returns a delete builder for the given id.
func (c *UserClient) DeleteOneID(id int) *UserDeleteOne {
	builder := c.Delete().Where(user.ID(id))
	builder.mutation.id = &id
	builder.mutation.op = OpDeleteOne
	return &UserDeleteOne{builder}
}

// Query returns a query builder for User.
func (c *UserClient) Query() *UserQuery {
	return &UserQuery{
		config: c.config,
	}
}

// Get returns a User entity by its id.
func (c *UserClient) Get(ctx context.Context, id int) (*User, error) {
	return c.Query().Where(user.ID(id)).Only(ctx)
}

// GetX is like Get, but panics if an error occurs.
func (c *UserClient) GetX(ctx context.Context, id int) *User {
	obj, err := c.Get(ctx, id)
	if err != nil {
		panic(err)
	}
	return obj
}

// Hooks returns the client hooks.
func (c *UserClient) Hooks() []Hook {
	return c.hooks.User
}
