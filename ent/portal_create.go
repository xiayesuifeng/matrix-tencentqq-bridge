// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/portal"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/schema"
	"maunium.net/go/mautrix/id"
)

// PortalCreate is the builder for creating a Portal entity.
type PortalCreate struct {
	config
	mutation *PortalMutation
	hooks    []Hook
}

// SetMatrixID sets the "matrix_id" field.
func (pc *PortalCreate) SetMatrixID(ii id.RoomID) *PortalCreate {
	pc.mutation.SetMatrixID(ii)
	return pc
}

// SetNillableMatrixID sets the "matrix_id" field if the given value is not nil.
func (pc *PortalCreate) SetNillableMatrixID(ii *id.RoomID) *PortalCreate {
	if ii != nil {
		pc.SetMatrixID(*ii)
	}
	return pc
}

// SetName sets the "name" field.
func (pc *PortalCreate) SetName(s string) *PortalCreate {
	pc.mutation.SetName(s)
	return pc
}

// SetNillableName sets the "name" field if the given value is not nil.
func (pc *PortalCreate) SetNillableName(s *string) *PortalCreate {
	if s != nil {
		pc.SetName(*s)
	}
	return pc
}

// SetType sets the "type" field.
func (pc *PortalCreate) SetType(st schema.PortalType) *PortalCreate {
	pc.mutation.SetType(st)
	return pc
}

// SetQqID sets the "qq_id" field.
func (pc *PortalCreate) SetQqID(i int64) *PortalCreate {
	pc.mutation.SetQqID(i)
	return pc
}

// SetGroupAvatarMd5 sets the "group_avatar_md5" field.
func (pc *PortalCreate) SetGroupAvatarMd5(s string) *PortalCreate {
	pc.mutation.SetGroupAvatarMd5(s)
	return pc
}

// SetNillableGroupAvatarMd5 sets the "group_avatar_md5" field if the given value is not nil.
func (pc *PortalCreate) SetNillableGroupAvatarMd5(s *string) *PortalCreate {
	if s != nil {
		pc.SetGroupAvatarMd5(*s)
	}
	return pc
}

// Mutation returns the PortalMutation object of the builder.
func (pc *PortalCreate) Mutation() *PortalMutation {
	return pc.mutation
}

// Save creates the Portal in the database.
func (pc *PortalCreate) Save(ctx context.Context) (*Portal, error) {
	var (
		err  error
		node *Portal
	)
	if len(pc.hooks) == 0 {
		if err = pc.check(); err != nil {
			return nil, err
		}
		node, err = pc.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*PortalMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = pc.check(); err != nil {
				return nil, err
			}
			pc.mutation = mutation
			if node, err = pc.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(pc.hooks) - 1; i >= 0; i-- {
			if pc.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = pc.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, pc.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (pc *PortalCreate) SaveX(ctx context.Context) *Portal {
	v, err := pc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (pc *PortalCreate) Exec(ctx context.Context) error {
	_, err := pc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (pc *PortalCreate) ExecX(ctx context.Context) {
	if err := pc.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (pc *PortalCreate) check() error {
	if _, ok := pc.mutation.GetType(); !ok {
		return &ValidationError{Name: "type", err: errors.New(`ent: missing required field "Portal.type"`)}
	}
	if _, ok := pc.mutation.QqID(); !ok {
		return &ValidationError{Name: "qq_id", err: errors.New(`ent: missing required field "Portal.qq_id"`)}
	}
	return nil
}

func (pc *PortalCreate) sqlSave(ctx context.Context) (*Portal, error) {
	_node, _spec := pc.createSpec()
	if err := sqlgraph.CreateNode(ctx, pc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{err.Error(), err}
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	return _node, nil
}

func (pc *PortalCreate) createSpec() (*Portal, *sqlgraph.CreateSpec) {
	var (
		_node = &Portal{config: pc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: portal.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: portal.FieldID,
			},
		}
	)
	if value, ok := pc.mutation.MatrixID(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: portal.FieldMatrixID,
		})
		_node.MatrixID = value
	}
	if value, ok := pc.mutation.Name(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: portal.FieldName,
		})
		_node.Name = value
	}
	if value, ok := pc.mutation.GetType(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeUint,
			Value:  value,
			Column: portal.FieldType,
		})
		_node.Type = value
	}
	if value, ok := pc.mutation.QqID(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Value:  value,
			Column: portal.FieldQqID,
		})
		_node.QqID = value
	}
	if value, ok := pc.mutation.GroupAvatarMd5(); ok {
		_spec.Fields = append(_spec.Fields, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: portal.FieldGroupAvatarMd5,
		})
		_node.GroupAvatarMd5 = value
	}
	return _node, _spec
}

// PortalCreateBulk is the builder for creating many Portal entities in bulk.
type PortalCreateBulk struct {
	config
	builders []*PortalCreate
}

// Save creates the Portal entities in the database.
func (pcb *PortalCreateBulk) Save(ctx context.Context) ([]*Portal, error) {
	specs := make([]*sqlgraph.CreateSpec, len(pcb.builders))
	nodes := make([]*Portal, len(pcb.builders))
	mutators := make([]Mutator, len(pcb.builders))
	for i := range pcb.builders {
		func(i int, root context.Context) {
			builder := pcb.builders[i]
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*PortalMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, pcb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, pcb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{err.Error(), err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				mutation.done = true
				if specs[i].ID.Value != nil {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, pcb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (pcb *PortalCreateBulk) SaveX(ctx context.Context) []*Portal {
	v, err := pcb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (pcb *PortalCreateBulk) Exec(ctx context.Context) error {
	_, err := pcb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (pcb *PortalCreateBulk) ExecX(ctx context.Context) {
	if err := pcb.Exec(ctx); err != nil {
		panic(err)
	}
}
