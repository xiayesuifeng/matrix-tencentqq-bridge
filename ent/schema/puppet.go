package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"maunium.net/go/mautrix/id"
)

// Puppet holds the schema definition for the Puppet entity.
type Puppet struct {
	ent.Schema
}

// Fields of the Puppet.
func (Puppet) Fields() []ent.Field {
	return []ent.Field{
		field.String("custom_matrix_id").
			GoType(id.UserID("")).
			Optional(),
		field.String("avatar_url").
			Default(""),
		field.Int64("uin"),
		field.String("display_name").
			Optional(),
	}
}

// Edges of the Puppet.
func (Puppet) Edges() []ent.Edge {
	return nil
}
