package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
	"maunium.net/go/mautrix/id"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

// Fields of the User.
func (User) Fields() []ent.Field {
	return []ent.Field{
		field.String("matrix_id").
			GoType(id.UserID("")).
			Unique(),
		field.String("matrix_token").
			Optional(),
		field.Int64("QQ").
			Optional(),
		field.Bytes("QQ_token").
			Optional(),
		field.String("space_room").
			GoType(id.RoomID("")).
			Optional(),
		field.String("management_room").
			GoType(id.RoomID("")).
			Optional(),
		field.Bytes("device_info").
			Optional(),
	}
}

// Edges of the User.
func (User) Edges() []ent.Edge {
	return nil
}

// Mixin of the User.
func (User) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixin.Time{},
	}
}
