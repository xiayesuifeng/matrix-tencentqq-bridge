package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
	"maunium.net/go/mautrix/id"
)

// Message holds the schema definition for the Message entity.
type Message struct {
	ent.Schema
}

// Indexes of the Message.
func (Message) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("msg_id", "portal_matrix_id").
			Unique(),
	}
}

// Fields of the Message.
func (Message) Fields() []ent.Field {
	return []ent.Field{
		field.Int32("msg_id"),
		field.String("portal_matrix_id").
			GoType(id.RoomID("")),
		field.JSON("event_id", []id.EventID{}),
	}
}

// Edges of the Message.
func (Message) Edges() []ent.Edge {
	return nil
}
