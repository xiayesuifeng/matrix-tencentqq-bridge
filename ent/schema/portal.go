package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"maunium.net/go/mautrix/id"
)

// Portal holds the schema definition for the Portal entity.
type Portal struct {
	ent.Schema
}

type PortalType uint

const (
	PrivatePortalType PortalType = iota
	TempPortalType
	GroupPortalType
)

// Fields of the Portal.
func (Portal) Fields() []ent.Field {
	return []ent.Field{
		field.String("matrix_id").
			GoType(id.RoomID("")).
			Optional(),
		field.String("name").
			Optional(),
		field.Uint("type").
			GoType(PortalType(0)),
		// uin or groupCode
		field.Int64("qq_id").
			Unique(),
		field.String("group_avatar_md5").
			Optional(),
	}
}

// Edges of the Portal.
func (Portal) Edges() []ent.Edge {
	return nil
}
