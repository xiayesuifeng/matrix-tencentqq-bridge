// Code generated by entc, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/predicate"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/user"
	"maunium.net/go/mautrix/id"
)

// UserUpdate is the builder for updating User entities.
type UserUpdate struct {
	config
	hooks    []Hook
	mutation *UserMutation
}

// Where appends a list predicates to the UserUpdate builder.
func (uu *UserUpdate) Where(ps ...predicate.User) *UserUpdate {
	uu.mutation.Where(ps...)
	return uu
}

// SetUpdateTime sets the "update_time" field.
func (uu *UserUpdate) SetUpdateTime(t time.Time) *UserUpdate {
	uu.mutation.SetUpdateTime(t)
	return uu
}

// SetMatrixID sets the "matrix_id" field.
func (uu *UserUpdate) SetMatrixID(ii id.UserID) *UserUpdate {
	uu.mutation.SetMatrixID(ii)
	return uu
}

// SetMatrixToken sets the "matrix_token" field.
func (uu *UserUpdate) SetMatrixToken(s string) *UserUpdate {
	uu.mutation.SetMatrixToken(s)
	return uu
}

// SetNillableMatrixToken sets the "matrix_token" field if the given value is not nil.
func (uu *UserUpdate) SetNillableMatrixToken(s *string) *UserUpdate {
	if s != nil {
		uu.SetMatrixToken(*s)
	}
	return uu
}

// ClearMatrixToken clears the value of the "matrix_token" field.
func (uu *UserUpdate) ClearMatrixToken() *UserUpdate {
	uu.mutation.ClearMatrixToken()
	return uu
}

// SetQQ sets the "QQ" field.
func (uu *UserUpdate) SetQQ(i int64) *UserUpdate {
	uu.mutation.ResetQQ()
	uu.mutation.SetQQ(i)
	return uu
}

// SetNillableQQ sets the "QQ" field if the given value is not nil.
func (uu *UserUpdate) SetNillableQQ(i *int64) *UserUpdate {
	if i != nil {
		uu.SetQQ(*i)
	}
	return uu
}

// AddQQ adds i to the "QQ" field.
func (uu *UserUpdate) AddQQ(i int64) *UserUpdate {
	uu.mutation.AddQQ(i)
	return uu
}

// ClearQQ clears the value of the "QQ" field.
func (uu *UserUpdate) ClearQQ() *UserUpdate {
	uu.mutation.ClearQQ()
	return uu
}

// SetQQToken sets the "QQ_token" field.
func (uu *UserUpdate) SetQQToken(b []byte) *UserUpdate {
	uu.mutation.SetQQToken(b)
	return uu
}

// ClearQQToken clears the value of the "QQ_token" field.
func (uu *UserUpdate) ClearQQToken() *UserUpdate {
	uu.mutation.ClearQQToken()
	return uu
}

// SetSpaceRoom sets the "space_room" field.
func (uu *UserUpdate) SetSpaceRoom(ii id.RoomID) *UserUpdate {
	uu.mutation.SetSpaceRoom(ii)
	return uu
}

// SetNillableSpaceRoom sets the "space_room" field if the given value is not nil.
func (uu *UserUpdate) SetNillableSpaceRoom(ii *id.RoomID) *UserUpdate {
	if ii != nil {
		uu.SetSpaceRoom(*ii)
	}
	return uu
}

// ClearSpaceRoom clears the value of the "space_room" field.
func (uu *UserUpdate) ClearSpaceRoom() *UserUpdate {
	uu.mutation.ClearSpaceRoom()
	return uu
}

// SetManagementRoom sets the "management_room" field.
func (uu *UserUpdate) SetManagementRoom(ii id.RoomID) *UserUpdate {
	uu.mutation.SetManagementRoom(ii)
	return uu
}

// SetNillableManagementRoom sets the "management_room" field if the given value is not nil.
func (uu *UserUpdate) SetNillableManagementRoom(ii *id.RoomID) *UserUpdate {
	if ii != nil {
		uu.SetManagementRoom(*ii)
	}
	return uu
}

// ClearManagementRoom clears the value of the "management_room" field.
func (uu *UserUpdate) ClearManagementRoom() *UserUpdate {
	uu.mutation.ClearManagementRoom()
	return uu
}

// SetDeviceInfo sets the "device_info" field.
func (uu *UserUpdate) SetDeviceInfo(b []byte) *UserUpdate {
	uu.mutation.SetDeviceInfo(b)
	return uu
}

// ClearDeviceInfo clears the value of the "device_info" field.
func (uu *UserUpdate) ClearDeviceInfo() *UserUpdate {
	uu.mutation.ClearDeviceInfo()
	return uu
}

// Mutation returns the UserMutation object of the builder.
func (uu *UserUpdate) Mutation() *UserMutation {
	return uu.mutation
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (uu *UserUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	uu.defaults()
	if len(uu.hooks) == 0 {
		affected, err = uu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*UserMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			uu.mutation = mutation
			affected, err = uu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(uu.hooks) - 1; i >= 0; i-- {
			if uu.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = uu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, uu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (uu *UserUpdate) SaveX(ctx context.Context) int {
	affected, err := uu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (uu *UserUpdate) Exec(ctx context.Context) error {
	_, err := uu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (uu *UserUpdate) ExecX(ctx context.Context) {
	if err := uu.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (uu *UserUpdate) defaults() {
	if _, ok := uu.mutation.UpdateTime(); !ok {
		v := user.UpdateDefaultUpdateTime()
		uu.mutation.SetUpdateTime(v)
	}
}

func (uu *UserUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   user.Table,
			Columns: user.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: user.FieldID,
			},
		},
	}
	if ps := uu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := uu.mutation.UpdateTime(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: user.FieldUpdateTime,
		})
	}
	if value, ok := uu.mutation.MatrixID(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldMatrixID,
		})
	}
	if value, ok := uu.mutation.MatrixToken(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldMatrixToken,
		})
	}
	if uu.mutation.MatrixTokenCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldMatrixToken,
		})
	}
	if value, ok := uu.mutation.QQ(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Value:  value,
			Column: user.FieldQQ,
		})
	}
	if value, ok := uu.mutation.AddedQQ(); ok {
		_spec.Fields.Add = append(_spec.Fields.Add, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Value:  value,
			Column: user.FieldQQ,
		})
	}
	if uu.mutation.QQCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Column: user.FieldQQ,
		})
	}
	if value, ok := uu.mutation.QQToken(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Value:  value,
			Column: user.FieldQQToken,
		})
	}
	if uu.mutation.QQTokenCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Column: user.FieldQQToken,
		})
	}
	if value, ok := uu.mutation.SpaceRoom(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldSpaceRoom,
		})
	}
	if uu.mutation.SpaceRoomCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldSpaceRoom,
		})
	}
	if value, ok := uu.mutation.ManagementRoom(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldManagementRoom,
		})
	}
	if uu.mutation.ManagementRoomCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldManagementRoom,
		})
	}
	if value, ok := uu.mutation.DeviceInfo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Value:  value,
			Column: user.FieldDeviceInfo,
		})
	}
	if uu.mutation.DeviceInfoCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Column: user.FieldDeviceInfo,
		})
	}
	if n, err = sqlgraph.UpdateNodes(ctx, uu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{user.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{err.Error(), err}
		}
		return 0, err
	}
	return n, nil
}

// UserUpdateOne is the builder for updating a single User entity.
type UserUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *UserMutation
}

// SetUpdateTime sets the "update_time" field.
func (uuo *UserUpdateOne) SetUpdateTime(t time.Time) *UserUpdateOne {
	uuo.mutation.SetUpdateTime(t)
	return uuo
}

// SetMatrixID sets the "matrix_id" field.
func (uuo *UserUpdateOne) SetMatrixID(ii id.UserID) *UserUpdateOne {
	uuo.mutation.SetMatrixID(ii)
	return uuo
}

// SetMatrixToken sets the "matrix_token" field.
func (uuo *UserUpdateOne) SetMatrixToken(s string) *UserUpdateOne {
	uuo.mutation.SetMatrixToken(s)
	return uuo
}

// SetNillableMatrixToken sets the "matrix_token" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableMatrixToken(s *string) *UserUpdateOne {
	if s != nil {
		uuo.SetMatrixToken(*s)
	}
	return uuo
}

// ClearMatrixToken clears the value of the "matrix_token" field.
func (uuo *UserUpdateOne) ClearMatrixToken() *UserUpdateOne {
	uuo.mutation.ClearMatrixToken()
	return uuo
}

// SetQQ sets the "QQ" field.
func (uuo *UserUpdateOne) SetQQ(i int64) *UserUpdateOne {
	uuo.mutation.ResetQQ()
	uuo.mutation.SetQQ(i)
	return uuo
}

// SetNillableQQ sets the "QQ" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableQQ(i *int64) *UserUpdateOne {
	if i != nil {
		uuo.SetQQ(*i)
	}
	return uuo
}

// AddQQ adds i to the "QQ" field.
func (uuo *UserUpdateOne) AddQQ(i int64) *UserUpdateOne {
	uuo.mutation.AddQQ(i)
	return uuo
}

// ClearQQ clears the value of the "QQ" field.
func (uuo *UserUpdateOne) ClearQQ() *UserUpdateOne {
	uuo.mutation.ClearQQ()
	return uuo
}

// SetQQToken sets the "QQ_token" field.
func (uuo *UserUpdateOne) SetQQToken(b []byte) *UserUpdateOne {
	uuo.mutation.SetQQToken(b)
	return uuo
}

// ClearQQToken clears the value of the "QQ_token" field.
func (uuo *UserUpdateOne) ClearQQToken() *UserUpdateOne {
	uuo.mutation.ClearQQToken()
	return uuo
}

// SetSpaceRoom sets the "space_room" field.
func (uuo *UserUpdateOne) SetSpaceRoom(ii id.RoomID) *UserUpdateOne {
	uuo.mutation.SetSpaceRoom(ii)
	return uuo
}

// SetNillableSpaceRoom sets the "space_room" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableSpaceRoom(ii *id.RoomID) *UserUpdateOne {
	if ii != nil {
		uuo.SetSpaceRoom(*ii)
	}
	return uuo
}

// ClearSpaceRoom clears the value of the "space_room" field.
func (uuo *UserUpdateOne) ClearSpaceRoom() *UserUpdateOne {
	uuo.mutation.ClearSpaceRoom()
	return uuo
}

// SetManagementRoom sets the "management_room" field.
func (uuo *UserUpdateOne) SetManagementRoom(ii id.RoomID) *UserUpdateOne {
	uuo.mutation.SetManagementRoom(ii)
	return uuo
}

// SetNillableManagementRoom sets the "management_room" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableManagementRoom(ii *id.RoomID) *UserUpdateOne {
	if ii != nil {
		uuo.SetManagementRoom(*ii)
	}
	return uuo
}

// ClearManagementRoom clears the value of the "management_room" field.
func (uuo *UserUpdateOne) ClearManagementRoom() *UserUpdateOne {
	uuo.mutation.ClearManagementRoom()
	return uuo
}

// SetDeviceInfo sets the "device_info" field.
func (uuo *UserUpdateOne) SetDeviceInfo(b []byte) *UserUpdateOne {
	uuo.mutation.SetDeviceInfo(b)
	return uuo
}

// ClearDeviceInfo clears the value of the "device_info" field.
func (uuo *UserUpdateOne) ClearDeviceInfo() *UserUpdateOne {
	uuo.mutation.ClearDeviceInfo()
	return uuo
}

// Mutation returns the UserMutation object of the builder.
func (uuo *UserUpdateOne) Mutation() *UserMutation {
	return uuo.mutation
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (uuo *UserUpdateOne) Select(field string, fields ...string) *UserUpdateOne {
	uuo.fields = append([]string{field}, fields...)
	return uuo
}

// Save executes the query and returns the updated User entity.
func (uuo *UserUpdateOne) Save(ctx context.Context) (*User, error) {
	var (
		err  error
		node *User
	)
	uuo.defaults()
	if len(uuo.hooks) == 0 {
		node, err = uuo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*UserMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			uuo.mutation = mutation
			node, err = uuo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(uuo.hooks) - 1; i >= 0; i-- {
			if uuo.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = uuo.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, uuo.mutation); err != nil {
			return nil, err
		}
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (uuo *UserUpdateOne) SaveX(ctx context.Context) *User {
	node, err := uuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (uuo *UserUpdateOne) Exec(ctx context.Context) error {
	_, err := uuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (uuo *UserUpdateOne) ExecX(ctx context.Context) {
	if err := uuo.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (uuo *UserUpdateOne) defaults() {
	if _, ok := uuo.mutation.UpdateTime(); !ok {
		v := user.UpdateDefaultUpdateTime()
		uuo.mutation.SetUpdateTime(v)
	}
}

func (uuo *UserUpdateOne) sqlSave(ctx context.Context) (_node *User, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   user.Table,
			Columns: user.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: user.FieldID,
			},
		},
	}
	id, ok := uuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "User.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := uuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, user.FieldID)
		for _, f := range fields {
			if !user.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != user.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := uuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := uuo.mutation.UpdateTime(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeTime,
			Value:  value,
			Column: user.FieldUpdateTime,
		})
	}
	if value, ok := uuo.mutation.MatrixID(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldMatrixID,
		})
	}
	if value, ok := uuo.mutation.MatrixToken(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldMatrixToken,
		})
	}
	if uuo.mutation.MatrixTokenCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldMatrixToken,
		})
	}
	if value, ok := uuo.mutation.QQ(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Value:  value,
			Column: user.FieldQQ,
		})
	}
	if value, ok := uuo.mutation.AddedQQ(); ok {
		_spec.Fields.Add = append(_spec.Fields.Add, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Value:  value,
			Column: user.FieldQQ,
		})
	}
	if uuo.mutation.QQCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeInt64,
			Column: user.FieldQQ,
		})
	}
	if value, ok := uuo.mutation.QQToken(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Value:  value,
			Column: user.FieldQQToken,
		})
	}
	if uuo.mutation.QQTokenCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Column: user.FieldQQToken,
		})
	}
	if value, ok := uuo.mutation.SpaceRoom(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldSpaceRoom,
		})
	}
	if uuo.mutation.SpaceRoomCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldSpaceRoom,
		})
	}
	if value, ok := uuo.mutation.ManagementRoom(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Value:  value,
			Column: user.FieldManagementRoom,
		})
	}
	if uuo.mutation.ManagementRoomCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeString,
			Column: user.FieldManagementRoom,
		})
	}
	if value, ok := uuo.mutation.DeviceInfo(); ok {
		_spec.Fields.Set = append(_spec.Fields.Set, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Value:  value,
			Column: user.FieldDeviceInfo,
		})
	}
	if uuo.mutation.DeviceInfoCleared() {
		_spec.Fields.Clear = append(_spec.Fields.Clear, &sqlgraph.FieldSpec{
			Type:   field.TypeBytes,
			Column: user.FieldDeviceInfo,
		})
	}
	_node = &User{config: uuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, uuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{user.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{err.Error(), err}
		}
		return nil, err
	}
	return _node, nil
}
