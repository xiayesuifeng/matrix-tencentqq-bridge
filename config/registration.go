package config

import (
	"fmt"
	"maunium.net/go/mautrix/appservice"
	"regexp"
)

func (c *Config) NewRegistration() (*appservice.Registration, error) {
	registration := appservice.CreateRegistration()

	err := c.copyToRegistration(registration)
	if err != nil {
		return nil, err
	}

	c.AppService.ASToken = registration.AppToken
	c.AppService.HSToken = registration.ServerToken

	// Workaround for https://github.com/matrix-org/synapse/pull/5758
	registration.SenderLocalpart = appservice.RandomString(32)
	botRegex := regexp.MustCompile(fmt.Sprintf("^@%s:%s$", c.AppService.Bot.Username, c.Homeserver.Domain))
	registration.Namespaces.RegisterUserIDs(botRegex, true)

	return registration, nil
}

func (c *Config) GetRegistration() (*appservice.Registration, error) {
	registration := appservice.CreateRegistration()

	err := c.copyToRegistration(registration)
	if err != nil {
		return nil, err
	}

	registration.AppToken = c.AppService.ASToken
	registration.ServerToken = c.AppService.HSToken
	return registration, nil
}

func (c *Config) copyToRegistration(registration *appservice.Registration) error {
	registration.ID = c.AppService.ID
	registration.URL = c.AppService.Address
	falseVal := false
	registration.RateLimited = &falseVal
	registration.SenderLocalpart = c.AppService.Bot.Username
	registration.EphemeralEvents = c.AppService.EphemeralEvents

	userIDRegex, err := regexp.Compile(fmt.Sprintf("^@%s:%s$",
		c.Bridge.FormatUsername(".+"),
		c.Homeserver.Domain))
	if err != nil {
		return err
	}
	registration.Namespaces.RegisterUserIDs(userIDRegex, true)
	return nil
}
