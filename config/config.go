package config

import (
	"gopkg.in/yaml.v3"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/id"
	"os"
	"strings"
	"text/template"
)

type Config struct {
	Homeserver Homeserver `yaml:"homeserver"`
	AppService struct {
		Address  string `yaml:"address"`
		Hostname string `yaml:"hostname"`
		Port     uint16 `yaml:"port"`

		Database DatabaseConfig `yaml:"database"`

		Provisioning struct {
			Prefix       string `yaml:"prefix"`
			SharedSecret string `yaml:"shared_secret"`
			SegmentKey   string `yaml:"segment_key"`
		} `yaml:"provisioning"`

		ID  string `yaml:"id"`
		Bot struct {
			Username    string `yaml:"username"`
			Displayname string `yaml:"displayname"`
			Avatar      string `yaml:"avatar"`

			ParsedAvatar id.ContentURI `yaml:"-"`
		} `yaml:"bot"`

		EphemeralEvents bool `yaml:"ephemeral_events"`

		ASToken string `yaml:"as_token"`
		HSToken string `yaml:"hs_token"`
	} `yaml:"appservice"`

	Bridge BridgeConfig `yaml:"bridge"`
}

type BridgeConfig struct {
	UsernameTemplate    string `yaml:"username_template"`
	DisplaynameTemplate string `yaml:"displayname_template"`
	FederateRooms       bool   `yaml:"federate_rooms"`

	PersonalFilteringSpaces bool `yaml:"personal_filtering_spaces"`

	PortalMessageBuffer int `yaml:"portal_message_buffer"`

	CommandPrefix string `yaml:"command_prefix"`

	ManagementRoomText struct {
		Welcome            string `yaml:"welcome"`
		WelcomeConnected   string `yaml:"welcome_connected"`
		WelcomeUnconnected string `yaml:"welcome_unconnected"`
		AdditionalHelp     string `yaml:"additional_help"`
	} `yaml:"management_room_text"`

	usernameTemplate    *template.Template `yaml:"-"`
	displaynameTemplate *template.Template `yaml:"-"`

	MaxGroupMemberSyncLimit uint `yaml:"max_group_member_sync_limit"`
}

type Homeserver struct {
	Address                       string `yaml:"address"`
	Domain                        string `yaml:"domain"`
	Asmux                         bool   `yaml:"asmux"`
	StatusEndpoint                string `yaml:"status_endpoint"`
	MessageSendCheckpointEndpoint string `yaml:"message_send_checkpoint_endpoint"`
	AsyncMedia                    bool   `yaml:"async_media"`
}

type DatabaseConfig struct {
	Type string `yaml:"type"`
	URI  string `yaml:"uri"`
}

func (c *Config) NewAppService() (*appservice.AppService, error) {
	as := appservice.Create()
	as.HomeserverDomain = c.Homeserver.Domain
	as.HomeserverURL = c.Homeserver.Address
	as.Host.Hostname = c.AppService.Hostname
	as.Host.Port = c.AppService.Port
	as.MessageSendCheckpointEndpoint = c.Homeserver.MessageSendCheckpointEndpoint
	as.DefaultHTTPRetries = 4
	var err error
	as.Registration, err = c.GetRegistration()
	return as, err
}

func Parse(data []byte) (*Config, error) {
	var config = &Config{}

	err := yaml.Unmarshal(data, config)
	if err != nil {
		return nil, err
	}

	config.Bridge.usernameTemplate, err = template.New("username").Parse(config.Bridge.UsernameTemplate)
	if err != nil {
		return nil, err
	}

	config.Bridge.displaynameTemplate, err = template.New("displayname").Parse(config.Bridge.DisplaynameTemplate)
	if err != nil {
		return nil, err
	}

	return config, err
}

func (c *Config) Save(path string) error {
	bytes, err := yaml.Marshal(c)
	if err != nil {
		return err
	}

	return os.WriteFile(path, bytes, 0644)
}

func (bc BridgeConfig) FormatUsername(username string) string {
	var buf strings.Builder
	_ = bc.usernameTemplate.Execute(&buf, username)
	return buf.String()
}

func (bc BridgeConfig) FormatDisplayName(displayName string) string {
	var buf strings.Builder
	_ = bc.displaynameTemplate.Execute(&buf, map[string]string{"DisplayName": displayName})
	return buf.String()
}
