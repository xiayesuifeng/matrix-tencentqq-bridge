package bridge

import (
	"context"
	"fmt"
	"github.com/Mrs4s/MiraiGo/client"
	"github.com/Mrs4s/MiraiGo/message"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/portal"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/schema"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/qq"
	"go.uber.org/zap"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
	"net/http"
	"strconv"
	"sync"
)

type Portal struct {
	*ent.Portal

	log *zap.Logger

	bridge *Bridge

	roomCreateLock sync.Mutex
	avatarLock     sync.Mutex

	messages       chan *PortalMessage
	matrixMessages chan *PortalMatrixMessage
}

func (b *Bridge) GetPortalByMXID(mxid id.RoomID) *Portal {
	b.portalsLock.Lock()
	defer b.portalsLock.Unlock()
	p, ok := b.portalsByMXID[mxid]
	if !ok {
		dbPortal, _ := b.db.Portal.Query().Where(portal.MatrixID(mxid)).Only(context.Background())

		return b.loadDBPortal(dbPortal, 0)
	}
	return p
}

func (b *Bridge) GetPortalByGroupCode(groupCode int64) *Portal {
	b.portalsLock.Lock()
	defer b.portalsLock.Unlock()
	p, ok := b.portalsByGroupCode[groupCode]
	if !ok {
		dbPortal, _ := b.db.Portal.Query().Where(portal.QqID(groupCode)).Only(context.Background())

		return b.loadDBPortal(dbPortal, groupCode)
	}
	return p
}

func (b *Bridge) loadDBPortal(dbPortal *ent.Portal, groupCode int64) *Portal {
	ctx := context.Background()

	if dbPortal == nil {
		if groupCode == 0 {
			return nil
		}

		var err error
		dbPortal, err = b.db.Portal.Create().SetType(schema.GroupPortalType).SetQqID(groupCode).Save(ctx)
		if err != nil {
			b.log.Error(err.Error())
			return nil
		}
	}

	p := b.NewPortal(dbPortal)
	b.portalsByGroupCode[groupCode] = p
	if len(p.MatrixID) > 0 {
		b.portalsByMXID[p.MatrixID] = p
	}

	return p
}

func (b *Bridge) NewPortal(dbPortal *ent.Portal) *Portal {
	p := &Portal{
		Portal: dbPortal,
		bridge: b,
		log:    b.log.Named(fmt.Sprintf("Portal/%d", dbPortal.QqID)),

		messages:       make(chan *PortalMessage, b.config.Bridge.PortalMessageBuffer),
		matrixMessages: make(chan *PortalMatrixMessage, b.config.Bridge.PortalMessageBuffer),
	}

	go p.handleMessageLoop(context.TODO())

	return p
}

func (p *Portal) MainIntent() *appservice.IntentAPI {
	return p.bridge.Bot
}

func (p *Portal) IsGroupPortal() bool {
	return p.Type == schema.GroupPortalType
}

func (p *Portal) getBridgeInfo() (string, event.BridgeEventContent) {
	bridgeInfo := event.BridgeEventContent{
		BridgeBot: p.bridge.Bot.UserID,
		Creator:   p.MainIntent().UserID,
		Protocol: event.BridgeInfoSection{
			ID:          "qq",
			DisplayName: "qq",
			AvatarURL:   p.bridge.config.AppService.Bot.ParsedAvatar.CUString(),
			ExternalURL: "https://im.qq.com/",
		},
		Channel: event.BridgeInfoSection{
			ID:          strconv.FormatInt(p.QqID, 10),
			DisplayName: p.Name,
		},
	}
	bridgeInfoStateKey := fmt.Sprintf("matrix.bridge.tencent.qq://qq/%d", p.QqID)
	return bridgeInfoStateKey, bridgeInfo
}

func (p *Portal) GetBasePowerLevels() *event.PowerLevelsEventContent {
	anyone := 0
	nope := 99
	invite := 50

	return &event.PowerLevelsEventContent{
		UsersDefault:    anyone,
		EventsDefault:   anyone,
		RedactPtr:       &anyone,
		StateDefaultPtr: &nope,
		BanPtr:          &nope,
		InvitePtr:       &invite,
		Users: map[id.UserID]int{
			p.MainIntent().UserID: 100,
		},
		Events: map[string]int{
			event.StateRoomName.Type:   anyone,
			event.StateRoomAvatar.Type: anyone,
			event.StateTopic.Type:      anyone,
		},
	}
}

func (p *Portal) CreateMatrixRoom(user *User, groupInfo *client.GroupInfo) error {
	p.roomCreateLock.Lock()
	defer p.roomCreateLock.Unlock()
	if len(p.MatrixID) > 0 {
		return nil
	}

	intent := p.MainIntent()
	if err := intent.EnsureRegistered(); err != nil {
		return err
	}

	p.log.Info("Creating Matrix room. Info source", zap.String("user", string(user.MatrixID)))

	if p.IsGroupPortal() {
		var err error
		if groupInfo == nil && p.IsGroupPortal() {
			groupInfo, err = user.client.GetGroupInfo(p.QqID)
			if err != nil {
				p.log.Warn("Failed to get group info", zap.Error(err))
				return err
			}
		}

		p.Name = groupInfo.Name
	}

	var invite []id.UserID

	bridgeInfoStateKey, bridgeInfo := p.getBridgeInfo()

	initialState := []*event.Event{{
		Type: event.StatePowerLevels,
		Content: event.Content{
			Parsed: p.GetBasePowerLevels(),
		},
	}, {
		Type:     event.StateBridge,
		Content:  event.Content{Parsed: bridgeInfo},
		StateKey: &bridgeInfoStateKey,
	}}

	creationContent := make(map[string]interface{})
	if !p.bridge.config.Bridge.FederateRooms {
		creationContent["m.federate"] = false
	}

	resp, err := intent.CreateRoom(&mautrix.ReqCreateRoom{
		Visibility:      "private",
		Name:            p.Name,
		Invite:          invite,
		Preset:          "private_chat",
		IsDirect:        !p.IsGroupPortal(),
		InitialState:    initialState,
		CreationContent: creationContent,
	})

	if err != nil {
		return err
	}

	p.Portal, err = p.Update().SetMatrixID(resp.RoomID).SetName(p.Name).Save(context.Background())
	if err != nil {
		p.log.Error("Failed to set portal matrix id", zap.Error(err))
		return err
	}
	p.bridge.portalsLock.Lock()
	p.bridge.portalsByMXID[p.MatrixID] = p
	p.bridge.portalsLock.Unlock()

	p.ensureUserInvited(user)

	go p.addToSpace(user)

	if p.IsGroupPortal() {
		members := groupInfo.Members
		if len(members) == 0 {
			members, err = user.client.GetGroupMembers(groupInfo)
			if err != nil {
				p.log.Warn("Failed to get member info", zap.Error(err))
				return err
			}
		}

		if err := p.UpdateAvatar(context.Background()); err != nil {
			p.log.Warn("Failed to update portal avatar", zap.Error(err))
		}

		go p.SyncGroupMembers(members)
	}

	return nil
}

type GroupMemberSyncQueue struct {
	queue          chan *GroupMemberSyncInfo
	cacheCount     uint
	coroutineCount uint
}

type GroupMemberSyncInfo struct {
	Member *client.GroupMemberInfo
	Portal *Portal
	WG     *sync.WaitGroup
}

var (
	groupMemberSyncQueue *GroupMemberSyncQueue
)

func initGroupMemberSyncQueue(cacheCount uint) {
	groupMemberSyncQueue = &GroupMemberSyncQueue{
		queue:      make(chan *GroupMemberSyncInfo, cacheCount),
		cacheCount: cacheCount,
	}
}

func getGroupMemberSyncQueue() *GroupMemberSyncQueue {
	return groupMemberSyncQueue
}

func destroyGroupMemberSyncQueue() {
	if groupMemberSyncQueue != nil {
		groupMemberSyncQueue.Close()
	}
}

func (g *GroupMemberSyncQueue) Add(portal *Portal, member *client.GroupMemberInfo, wg *sync.WaitGroup) {
	if g.cacheCount == 0 || g.coroutineCount < g.cacheCount {
		g.coroutineCount++
		go func() {
			for info := range g.queue {
				g.process(info)
			}
		}()
	}

	g.queue <- &GroupMemberSyncInfo{
		Portal: portal,
		Member: member,
		WG:     wg,
	}
}

func (g *GroupMemberSyncQueue) process(info *GroupMemberSyncInfo) {
	defer info.WG.Done()
	p := info.Portal
	member := info.Member
	puppet := p.bridge.GetPuppetByUin(member.Uin)
	if err := puppet.Sync(context.Background(), member.Nickname); err != nil {
		p.log.Error("Failed to ensure registered", zap.String("puppet_matrix_id", puppet.MatrixID.String()), zap.Error(err))
	}

	if !puppet.IntentFor(p).IsCustomPuppet {
		if err := puppet.IntentFor(p).EnsureJoined(p.MatrixID); err != nil {
			p.log.Error("Failed to make puppet join portal",
				zap.String("puppet_matrix_id", puppet.MatrixID.String()),
				zap.String("portal_matrix_id", p.MatrixID.String()),
				zap.Error(err))
		}
	}
}

func (g *GroupMemberSyncQueue) Close() {
	close(g.queue)
}

func (p *Portal) SyncGroupMembers(members []*client.GroupMemberInfo) {
	p.log.Info("Syncing Matrix group room members.", zap.Int64("groupCode", p.QqID))

	wg := sync.WaitGroup{}

	syncQueue := getGroupMemberSyncQueue()

	memberMap := make(map[int64]bool)

	for _, member := range members {
		wg.Add(1)
		syncQueue.Add(p, member, &wg)
		memberMap[member.Uin] = true
	}

	matrixMembers, err := p.MainIntent().JoinedMembers(p.MatrixID)

	wg.Wait()

	if err != nil {
		p.log.Warn("Failed to get member list",
			zap.String("matrix_matrix_id", p.MatrixID.String()),
			zap.Error(err))
	} else {
		// kick extra members
		for member := range matrixMembers.Joined {
			uin, ok := p.bridge.ParsePuppetMXID(member)
			if ok {
				_, exist := memberMap[uin]
				if !exist {
					if _, err = p.MainIntent().KickUser(p.MatrixID, &mautrix.ReqKickUser{
						UserID: member,
						Reason: "User had left this QQ group",
					}); err != nil {
						p.log.Warn("Failed to kick user who had left",
							zap.String("puppet_matrix_id", member.String()),
							zap.String("portal_matrix_id", p.MatrixID.String()),
							zap.Error(err))
					}
				}
			}
		}
	}

	p.log.Info("Syncing Matrix group room members done.", zap.Int64("groupCode", p.QqID))
}

func (p *Portal) UpdateAvatar(ctx context.Context) error {
	if len(p.MatrixID) == 0 || !p.IsGroupPortal() {
		return nil
	}

	p.avatarLock.Lock()
	defer p.avatarLock.Unlock()

	avatar, md5, err := qq.GetGroupAvatar(p.QqID)
	if err != nil {
		return err
	}

	if p.GroupAvatarMd5 != md5 {
		mediaUpload, err := p.MainIntent().UploadBytes(avatar, http.DetectContentType(avatar))
		if err != nil {
			return err
		}

		_, err = p.MainIntent().SetRoomAvatar(p.MatrixID, mediaUpload.ContentURI)
		if err != nil {
			return err
		}

		if p.Portal, err = p.Update().SetGroupAvatarMd5(md5).Save(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (p *Portal) UpdateMatrixRoom(user *User, groupInfo *client.GroupInfo) bool {
	if len(p.MatrixID) == 0 {
		return false
	}
	p.log.Info("Syncing portal", zap.String("user", user.MatrixID.String()))

	p.ensureUserInvited(user)
	go p.addToSpace(user)

	var err error
	if p.IsGroupPortal() {
		if groupInfo == nil && p.IsGroupPortal() {
			groupInfo, err = user.client.GetGroupInfo(p.QqID)
			if err != nil {
				p.log.Warn("Failed to get group info", zap.Error(err))
				return false
			}
		}

		if groupInfo.Name != p.Name {
			if p.Portal, err = p.Update().SetName(p.Name).Save(context.Background()); err != nil {
				p.log.Warn("Failed to update portal name to database", zap.Error(err))
			}
		}

		if err = p.UpdateAvatar(context.Background()); err != nil {
			p.log.Warn("Failed to update portal avatar", zap.Error(err))
		}

		members := groupInfo.Members
		if len(members) == 0 {
			members, err = user.client.GetGroupMembers(groupInfo)
			if err != nil {
				p.log.Warn("Failed to get member info", zap.Error(err))
				return false
			}
		}

		go p.SyncGroupMembers(members)
	}

	return true
}

func (p *Portal) ensureUserInvited(user *User) bool {
	return user.ensureInvited(p.MainIntent(), p.MatrixID, !p.IsGroupPortal())
}

func (p *Portal) handleMessageLoop(ctx context.Context) {
	for {
		select {
		case msg := <-p.messages:
			p.handleMessageLoopItem(ctx, msg)
		case msg := <-p.matrixMessages:
			p.handleMatrixMessageLoopItem(ctx, msg)
		}
	}
}

func (p *Portal) handleMessageLoopItem(ctx context.Context, msg *PortalMessage) {
	if len(p.MatrixID) == 0 {
		p.log.Debug("Creating Matrix room from incoming message")
		err := p.CreateMatrixRoom(msg.Source, nil)
		if err != nil {
			p.log.Error("Failed to create portal room", zap.Error(err))
			return
		}
	}

	switch originMsg := msg.Message.(type) {
	case *message.GroupMessage:
		puppet := p.bridge.GetPuppetByUin(originMsg.Sender.Uin)
		err := puppet.Sync(ctx, originMsg.Sender.Nickname)
		if err != nil {
			p.log.Error("Failed to sync puppet", zap.Error(err))
		}

		_, err = p.bridge.GetMessageByMsgID(ctx, originMsg.Id, p.MatrixID)
		if !ent.IsNotFound(err) {
			p.log.Debug("Message is duplicate,skip send message")
			return
		}

		evts := make([]id.EventID, 0)

		convertMessage := msg.convertMatrixMessage(ctx, p.MatrixID)

		if len(convertMessage.RelatesTo) != 0 {
			relatesEvt, err := p.MainIntent().GetEvent(p.MatrixID, convertMessage.RelatesTo)
			if err != nil {
				p.log.Error("Failed to get event",
					zap.String("event_id", convertMessage.RelatesTo.String()),
					zap.String("puppet_matrix_id", p.MatrixID.String()),
					zap.String("portal_matrix_id", p.MatrixID.String()),
					zap.Error(err))
			} else {
				convertMessage.PrimaryContent.SetReply(relatesEvt)
			}
		}

		matrixMessage := []*event.MessageEventContent{&convertMessage.PrimaryContent}
		matrixMessage = append(matrixMessage, convertMessage.ExtraContent...)
		for _, content := range matrixMessage {
			if content.MsgType == event.MsgImage || content.MsgType == event.MsgFile {
				mediaUpload, err := puppet.IntentFor(p).UploadLink(string(content.URL))
				if err != nil {
					p.log.Error("Failed to upload file or image",
						zap.String("puppet_matrix_id", p.MatrixID.String()),
						zap.String("portal_matrix_id", p.MatrixID.String()),
						zap.Error(err))

					continue
				}

				content.URL = mediaUpload.ContentURI.CUString()
			}

			if evt, err := p.sendMessage(puppet.IntentFor(p), event.EventMessage, content, msg.UnixMilliTime()); err != nil {
				p.log.Error("Failed to send message to portal room",
					zap.String("puppet_matrix_id", p.MatrixID.String()),
					zap.String("portal_matrix_id", p.MatrixID.String()),
					zap.Error(err))
			} else {
				evts = append(evts, evt.EventID)
			}
		}

		if len(evts) > 0 {
			_, err = p.bridge.CreateMessage(ctx, originMsg.Id, p.MatrixID, evts)
			if err != nil {
				p.log.Error("Failed to create message", zap.Error(err))
			}
		}
	}
}

func (p *Portal) handleMatrixMessageLoopItem(ctx context.Context, msg *PortalMatrixMessage) {
	elements, err := msg.convertMatrixMessage(ctx, p.MainIntent(), p.MatrixID, p.IsGroupPortal(), p.QqID)
	if err != nil {
		p.log.Error("Failed to convert matrix message to qq message", zap.Error(err))
		return
	}
	qMsg := msg.Source.client.SendGroupMessage(p.QqID, &message.SendingMessage{Elements: elements})
	if qMsg != nil && qMsg.Id != -1 {
		_, err := p.bridge.CreateMessage(ctx, qMsg.Id, p.MatrixID, []id.EventID{msg.Evt.ID})
		if err != nil {
			p.log.Error("Failed to create message", zap.Error(err))
		}
	} else {
		p.log.Error("Failed to send message to qq", zap.String("event_id", msg.Evt.ID.String()))
	}
}

func (p *Portal) sendMessage(intent *appservice.IntentAPI, eventType event.Type, content *event.MessageEventContent, timestamp int64) (*mautrix.RespSendEvent, error) {
	if timestamp == 0 {
		return intent.SendMessageEvent(p.MatrixID, eventType, content)
	} else {
		return intent.SendMassagedMessageEvent(p.MatrixID, eventType, content, timestamp)
	}
}

func (p *Portal) addToSpace(user *User) {
	spaceID := user.GetSpaceRoom()
	if len(spaceID) == 0 {
		return
	}
	_, err := p.MainIntent().SendStateEvent(spaceID, event.StateSpaceChild, p.MatrixID.String(), &event.SpaceChildEventContent{
		Via: []string{p.bridge.config.Homeserver.Domain},
	})
	if err != nil {
		p.log.Sugar().Errorf("Failed to add room to %s's personal filtering space (%s): %v", user.MatrixID, spaceID, err)
	} else {
		p.log.Sugar().Errorf("Added room to %s's personal filtering space (%s)", user.MatrixID, spaceID)
	}
}
