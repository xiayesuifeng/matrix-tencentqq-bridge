package bridge

import (
	"context"
	"fmt"
	"github.com/Mrs4s/MiraiGo/client"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var TimeoutErr = errors.New("timeout")

type CommandHandler struct {
	bridge *Bridge
	log    *zap.Logger
}

func NewCommandHandler(bridge *Bridge) *CommandHandler {
	return &CommandHandler{
		bridge: bridge,
		log:    bridge.log.Named("command_handler"),
	}
}

// CommandEvent stores all data which might be used to handle commands
type CommandEvent struct {
	Bot     *appservice.IntentAPI
	Bridge  *Bridge
	Handler *CommandHandler
	RoomID  id.RoomID
	EventID id.EventID
	User    *User
	Command string
	Args    []string
	ReplyTo id.EventID
}

// Reply sends a reply to command as notice
func (ce *CommandEvent) Reply(msg string, args ...interface{}) (id.EventID, error) {
	content := format.RenderMarkdown(fmt.Sprintf(msg, args...), true, false)
	content.MsgType = event.MsgNotice
	intent := ce.Bot

	evt, err := intent.SendMessageEvent(ce.RoomID, event.EventMessage, content)
	if err != nil {
		ce.Handler.log.Sugar().Warnf("Failed to reply to command from %s: %v", ce.User.MatrixID, err)
	}

	return evt.EventID, err
}

func (ce *CommandEvent) EditReply(original id.EventID, msg string, args ...interface{}) (id.EventID, error) {
	content := format.RenderMarkdown(fmt.Sprintf(msg, args...), true, false)
	content.MsgType = event.MsgNotice

	content.SetEdit(original)

	evt, err := ce.Bot.SendMessageEvent(ce.RoomID, event.EventMessage, content)
	if err != nil {
		ce.Handler.log.Sugar().Warnf("Failed to edit reply to command from %s,original eventID:%s, error: %v", ce.User.MatrixID, original, err)
	}

	return evt.EventID, err
}

func (ce *CommandEvent) ReplyImage(image []byte, body string) (id.EventID, error) {
	mediaUpload, err := ce.Bot.UploadBytes(image, http.DetectContentType(image))
	if err != nil {
		return "", err
	}

	evt, err := ce.Bot.SendImage(ce.RoomID, body, mediaUpload.ContentURI)
	return evt.EventID, err
}

func (ce *CommandEvent) WaitUserReply(timeout time.Duration) (*event.MessageEventContent, error) {
	ce.Bridge.matrixHandler.cmdSessionMutex.Lock()
	ce.Bridge.matrixHandler.cmdSession[ce.RoomID] = make(chan *event.Event)
	ce.Bridge.matrixHandler.cmdSessionMutex.Unlock()

	ctx := context.Background()
	if timeout != 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}

	select {
	case <-ctx.Done():
		return nil, TimeoutErr
	case e := <-ce.Bridge.matrixHandler.cmdSession[ce.RoomID]:
		close(ce.Bridge.matrixHandler.cmdSession[ce.RoomID])
		delete(ce.Bridge.matrixHandler.cmdSession, ce.RoomID)

		return e.Content.AsMessage(), nil
	}
}

// Handle handles messages to the bridge
func (handler *CommandHandler) Handle(roomID id.RoomID, eventID id.EventID, user *User, message string, replyTo id.EventID) {
	args := strings.Fields(message)
	if len(args) == 0 {
		args = []string{"unknown-command"}
	}
	ce := &CommandEvent{
		Bot:     handler.bridge.Bot,
		Bridge:  handler.bridge,
		Handler: handler,
		RoomID:  roomID,
		EventID: eventID,
		User:    user,
		Command: strings.ToLower(args[0]),
		Args:    args[1:],
		ReplyTo: replyTo,
	}

	handler.CommandMux(ce)
}

func (handler *CommandHandler) CommandMux(ce *CommandEvent) {
	switch ce.Command {
	case "login":
		handler.CommandLogin(ce)
	case "login-qr":
		handler.CommandLoginQr(ce)
	case "help":
		handler.CommandHelp(ce)
	default:
		ce.Reply("Unknown command, use the `help` command for help.")
	}
}

const cmdLoginHelp = `login - Link the bridge to your QQ account as a client`

// CommandLogin handles login command
func (handler *CommandHandler) CommandLogin(ce *CommandEvent) {
	timeout := time.Minute * 5

	ce.Reply("请输入 QQ 号")

	userReply, err := ce.WaitUserReply(timeout)
	if err == TimeoutErr {
		ce.Reply("登录超时")
		return
	}

	uid, err := strconv.ParseInt(userReply.Body, 10, 64)
	if err != nil {
		ce.Reply("QQ 号格式错误")
		return
	}

	ce.Reply("请输入密码")

	userReply, err = ce.WaitUserReply(timeout)
	if err == TimeoutErr {
		ce.Reply("登录超时")
		return
	}

	loginRsp, err := ce.User.Login(context.Background(), uid, userReply.Body)
	if err != nil {
		if err == client.ErrAlreadyOnline {
			ce.Reply("You are already logged in as %d.", ce.User.QQ)
		}
		ce.Reply(err.Error())
		return
	}

	if err := handler.loginResponseProcessor(timeout, loginRsp, ce); err != nil {
		ce.Reply("登录失败: %v", err.Error())
	} else {
		ce.User.SetQQAndToken(context.Background())
		ce.Reply("登录成功")
	}

	go ce.User.Connect()
}

const cmdLoginQrHelp = `login-qr - Use QR code login to link the bridge as a client to your QQ account`

// CommandLoginQr handles login-qr command
func (handler *CommandHandler) CommandLoginQr(ce *CommandEvent) {
	timeout := time.Minute * 5

	ctx := context.Background()

	err := ce.User.NewEmptyClient(ctx)
	if err != nil {
		if err == client.ErrAlreadyOnline {
			ce.Reply("You are already logged in as %d.", ce.User.QQ)
		} else {
			ce.Reply(err.Error())
		}
		return
	}

	qrRsp, err := ce.User.FetchQRCode()
	if err != nil {
		ce.Reply(err.Error())
		return
	}

	qrCodeTextEvt, err := ce.Reply("请扫描二维码.")
	if err != nil {
		handler.log.Error("send reply failed", zap.Error(err))
		return
	}

	qrCodeEvt, err := ce.ReplyImage(qrRsp.ImageData, "login qr code image")
	if err != nil {
		handler.log.Error("send qr code image failed", zap.Error(err))
		return
	}

	loginQrRsp, err := ce.User.QueryQRCodeStatus(qrRsp.Sig)
	if err != nil {
		ce.Reply("登录失败: %v", err.Error())
		return
	}

	timeoutCtx, cancel := context.WithTimeout(ctx, timeout*3)
	defer cancel()

	prevState := loginQrRsp.State
	for {
		select {
		case <-timeoutCtx.Done():
			ce.Reply("登录超时，退出登录.")
			return
		default:
			time.Sleep(time.Second)
			loginQrRsp, _ = ce.User.QueryQRCodeStatus(qrRsp.Sig)
			if loginQrRsp == nil {
				continue
			}
			if prevState == loginQrRsp.State {
				continue
			}
			prevState = loginQrRsp.State
			switch loginQrRsp.State {
			case client.QRCodeCanceled:
				ce.EditReply(qrCodeTextEvt, "扫码被用户取消.")
				return
			case client.QRCodeTimeout:
				ce.EditReply(qrCodeTextEvt, "二维码过期，请重新扫描.")
				qrRsp, err = ce.User.FetchQRCode()
				if err != nil {
					ce.Reply(err.Error())
					return
				}

				if qrCodeEvt == "" {
					qrCodeEvt, err = ce.ReplyImage(qrRsp.ImageData, "login qr code image")
					if err != nil {
						ce.Reply("登录失败: %v", err.Error())
						handler.log.Error("send qr code image failed", zap.Error(err))
						return
					}
				} else {
					ce.Bot.RedactEvent(ce.RoomID, qrCodeEvt)

					qrCodeEvt, err = ce.ReplyImage(qrRsp.ImageData, "login qr code image")
					if err != nil {
						ce.Reply("登录失败: %v", err.Error())
						handler.log.Error("send qr code image failed", zap.Error(err))
						return
					}
				}

				continue
			case client.QRCodeWaitingForConfirm:
				ce.Bot.RedactEvent(ce.RoomID, qrCodeEvt)
				qrCodeEvt = ""
				ce.EditReply(qrCodeTextEvt, "扫码成功, 请在手机端确认登录.")
			case client.QRCodeConfirmed:
				ce.EditReply(qrCodeTextEvt, "登录中.")
				loginRsp, err := ce.User.LoginQr(loginQrRsp.LoginInfo)
				if err != nil {
					ce.Reply("登录失败: %v", err.Error())
					return
				}

				if err := handler.loginResponseProcessor(timeout, loginRsp, ce); err != nil {
					ce.Reply("登录失败: %v", err.Error())
				} else {
					ce.User.SetQQAndToken(context.Background())
					ce.EditReply(qrCodeTextEvt, "登录成功.")

					go ce.User.Connect()
				}

				return
			case client.QRCodeImageFetch, client.QRCodeWaitingForScan:
				// ignore
			}
		}
	}
}

// loginResponseProcessor Login result processing
func (handler *CommandHandler) loginResponseProcessor(timeout time.Duration, rsp *client.LoginResponse, ce *CommandEvent) error {
	var err error
	c := ce.User.client

	for {
		if err != nil {
			return err
		}

		if rsp.Success {
			return nil
		}

		switch rsp.Error {
		case client.SliderNeededError:
			ce.Reply("登录需要滑条验证码,暂不支持,请使用 `login-qr` 指令用二维码登录.")
			return errors.New("登录需要滑条验证码")
		case client.NeedCaptcha:
			r, _ := ce.Bot.UploadBytes(rsp.CaptchaImage, "image/png")
			ce.Bot.SendImage(ce.RoomID, "请输入验证码.", r.ContentURI)
			userReply, err := ce.WaitUserReply(timeout)
			if err != nil {
				return err
			}
			rsp, _ = c.SubmitCaptcha(userReply.Body, rsp.CaptchaSign)
		case client.SMSNeededError:
			if !c.RequestSMS() {
				ce.Reply("账号已开启设备锁, 发送短信验证码失败.")
			}
			ce.Reply("账号已开启设备锁, 已向手机 %v 发送短信验证码,请输入.", rsp.SMSPhone)
			userReply, err := ce.WaitUserReply(timeout)
			if err != nil {
				return err
			}
			rsp, err = c.SubmitSMS(userReply.Body)
		case client.SMSOrVerifyNeededError:
			ce.Reply("账号已开启设备锁，请选择验证方式:")
			ce.Reply("1. 向手机 %v 发送短信验证码.", rsp.SMSPhone)
			ce.Reply("2. 使用手机 QQ 扫码验证.")
			ce.Reply("请输入(1 - 2)")
			userReply, err := ce.WaitUserReply(timeout)
			if err != nil {
				return err
			}
			if strings.Contains(userReply.Body, "1") {
				if !c.RequestSMS() {
					ce.Reply("发送验证码失败，可能是请求过于频繁.")
					return nil
				}

				ce.Reply("请输入短信验证码.")
				userReply, err = ce.WaitUserReply(timeout)
				if err != nil {
					return err
				}
				rsp, err = c.SubmitSMS(userReply.Body)
				continue
			}
			fallthrough
		case client.UnsafeDeviceError:
			ce.Reply("账号已开启设备锁，请前往 -> %v <- 验证后重新登录.", rsp.VerifyUrl)
			return nil
		case client.OtherLoginError, client.UnknownLoginError, client.TooManySMSRequestError:
			msg := rsp.ErrorMessage
			if strings.Contains(msg, "版本") {
				msg = "密码错误或账号被冻结"
			}
			if strings.Contains(msg, "冻结") {
				msg = "账号被冻结"
			}
			return errors.New(msg)
		}
	}
}

const cmdHelpHelp = `help - Prints this help`

// CommandHelp handles help command
func (handler *CommandHandler) CommandHelp(ce *CommandEvent) {
	cmdPrefix := ""
	if ce.User.ManagementRoom != ce.RoomID {
		cmdPrefix = handler.bridge.config.Bridge.CommandPrefix + " "
	}

	ce.Reply("* " + strings.Join([]string{
		cmdPrefix + cmdHelpHelp,
		cmdPrefix + cmdLoginHelp,
		cmdPrefix + cmdLoginQrHelp,
	}, "\n* "))
}
