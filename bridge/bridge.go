package bridge

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/config"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/database"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
	"go.uber.org/zap"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/id"
	"sync"
	"time"
)

type Bridge struct {
	AppService     *appservice.AppService
	EventProcessor *appservice.EventProcessor
	Bot            *appservice.IntentAPI

	db *ent.Client

	log *zap.Logger

	config *config.Config

	matrixHandler *MatrixHandler

	usersByMXID         map[id.UserID]*User
	usersLock           sync.Mutex
	managementRooms     map[id.RoomID]*User
	managementRoomsLock sync.Mutex

	portalsByMXID      map[id.RoomID]*Portal
	portalsByGroupCode map[int64]*Portal
	portalsLock        sync.Mutex

	puppets             map[int64]*Puppet
	puppetsByCustomMXID map[id.UserID]*Puppet
	puppetsLock         sync.Mutex
}

func NewBridge(conf *config.Config, log *zap.Logger) (*Bridge, error) {
	log.Info("Initializing database")
	client, err := database.NewClient(conf.AppService.Database)
	if err != nil {
		return nil, err
	}

	bridge := &Bridge{
		config:              conf,
		log:                 log,
		db:                  client,
		usersByMXID:         map[id.UserID]*User{},
		managementRooms:     map[id.RoomID]*User{},
		portalsByMXID:       map[id.RoomID]*Portal{},
		portalsByGroupCode:  map[int64]*Portal{},
		puppets:             map[int64]*Puppet{},
		puppetsByCustomMXID: map[id.UserID]*Puppet{},
	}

	appService, err := conf.NewAppService()
	if err != nil {
		return nil, err
	}

	bridge.AppService = appService

	_, err = appService.Init()
	if err != nil {
		return nil, err
	}

	bridge.Bot = appService.BotIntent()

	log.Info("Initializing Matrix event processor")
	bridge.EventProcessor = appservice.NewEventProcessor(bridge.AppService)

	log.Info("Initializing Matrix event handler")
	bridge.matrixHandler = NewMatrixHandler(bridge, log.Named("matrixHandler"))

	return bridge, nil
}

func (b *Bridge) Start() {
	b.ensureConnection()

	initGroupMemberSyncQueue(b.config.Bridge.MaxGroupMemberSyncLimit)

	b.log.Info("Starting application service HTTP server")
	go b.AppService.Start()
	b.log.Info("Starting event processor")
	go b.EventProcessor.Start()
	go b.UpdateBotProfile()

	go b.StartUsers(context.TODO())
}

func (b *Bridge) Stop() {
	b.AppService.Stop()
	b.EventProcessor.Stop()

	destroyGroupMemberSyncQueue()
}

func (b *Bridge) ensureConnection() {
	for {
		resp, err := b.Bot.Whoami()
		if err != nil {
			if errors.Is(err, mautrix.MUnknownToken) {
				b.log.Error("The as_token was not accepted. Is the registration file installed in your homeserver correctly?")
			} else if errors.Is(err, mautrix.MExclusive) {
				b.log.Error("The as_token was accepted, but the /register request was not. Are the homeserver domain and username template in the config correct, and do they match the values in the registration?")
			}
			b.log.Error("Failed to connect to homeserver: %v. Retrying in 10 seconds...", zap.Error(err))
			time.Sleep(10 * time.Second)
		} else if resp.UserID != b.Bot.UserID {
			b.log.Error(fmt.Sprintf("Unexpected user ID in whoami call: got %s, expected %s", resp.UserID, b.Bot.UserID))
		} else {
			break
		}
	}
}

func (b *Bridge) UpdateBotProfile() {
	b.log.Info("Updating bot profile")
	botConfig := &b.config.AppService.Bot

	var err error
	var mxc id.ContentURI
	if botConfig.Avatar == "remove" {
		err = b.Bot.SetAvatarURL(mxc)
	} else if len(botConfig.Avatar) > 0 {
		mxc, err = id.ParseContentURI(botConfig.Avatar)
		if err == nil {
			err = b.Bot.SetAvatarURL(mxc)
		}
		botConfig.ParsedAvatar = mxc
	}
	if err != nil {
		b.log.Error("Failed to update bot avatar", zap.Error(err))
	}

	if botConfig.Displayname == "remove" {
		err = b.Bot.SetDisplayName("")
	} else if len(botConfig.Displayname) > 0 {
		err = b.Bot.SetDisplayName(botConfig.Displayname)
	}
	if err != nil {
		b.log.Error("Failed to update bot displayname", zap.Error(err))
	}
}

func (b *Bridge) StartUsers(ctx context.Context) {
	b.log.Debug("Starting users")
	for _, user := range b.GetAllUsers(ctx) {
		if len(user.QQToken) > 0 {
			if err := user.LoginWithToken(ctx); err != nil {
				b.log.Error("user client login failed", zap.Error(err))
				continue
			}

			go user.Connect()
		}
	}
}
