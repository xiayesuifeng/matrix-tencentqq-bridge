package bridge

import (
	"context"
	"crypto/md5"
	"github.com/Mrs4s/MiraiGo/client"
	"github.com/Mrs4s/MiraiGo/message"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/user"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/qq"
	"go.uber.org/zap"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
	"strings"
	"sync"
)

type User struct {
	*ent.User

	log *zap.Logger

	bridge *Bridge

	client *qq.Client

	clientHandler *ClientHandler

	spaceCreateLock sync.Mutex
}

func (b *Bridge) GetUserByMXID(userID id.UserID) *User {
	if userID == b.Bot.UserID {
		return nil
	}
	b.usersLock.Lock()
	defer b.usersLock.Unlock()
	u, ok := b.usersByMXID[userID]
	if !ok {
		dbUser, err := b.db.User.Query().Where(user.MatrixID(userID)).Only(context.Background())
		if err != nil {
			if ent.IsNotFound(err) {
				dbUser, err = b.db.User.Create().SetMatrixID(userID).Save(context.Background())
				if err != nil {
					b.log.Error(err.Error())
					return nil
				}
			} else {
				b.log.Error(err.Error())
				return nil
			}
		}

		if len(dbUser.ManagementRoom) > 0 {
			b.managementRooms[dbUser.ManagementRoom] = u
		}

		return b.NewUser(dbUser)
	}
	return u
}

func (b *Bridge) GetAllUsers(ctx context.Context) (users []*User) {
	b.usersLock.Lock()
	defer b.usersLock.Unlock()
	dbUsers, err := b.db.User.Query().All(ctx)
	if err != nil {
		b.log.Error("get user list for database failed", zap.Error(err))
		return
	}

	for _, dbUser := range dbUsers {
		u, ok := b.usersByMXID[dbUser.MatrixID]
		if !ok {
			u = b.NewUser(dbUser)

			b.usersByMXID[dbUser.MatrixID] = u

			if len(dbUser.ManagementRoom) > 0 {
				b.managementRooms[dbUser.ManagementRoom] = u
			}
		}

		users = append(users, u)
	}

	return users
}

func (u *User) GetSpaceRoom() id.RoomID {
	if !u.bridge.config.Bridge.PersonalFilteringSpaces {
		return ""
	}

	u.spaceCreateLock.Lock()
	defer u.spaceCreateLock.Unlock()

	if len(u.SpaceRoom) == 0 {
		resp, err := u.bridge.Bot.CreateRoom(&mautrix.ReqCreateRoom{
			Visibility: "private",
			Name:       "QQ",
			Topic:      "Your Tencent QQ bridged chats",
			InitialState: []*event.Event{{
				Type: event.StateRoomAvatar,
				Content: event.Content{
					Parsed: &event.RoomAvatarEventContent{
						URL: u.bridge.config.AppService.Bot.ParsedAvatar,
					},
				},
			}},
			CreationContent: map[string]interface{}{
				"type": event.RoomTypeSpace,
			},
			PowerLevelOverride: &event.PowerLevelsEventContent{
				Users: map[id.UserID]int{
					u.bridge.Bot.UserID: 9001,
					u.MatrixID:          50,
				},
			},
		})

		if err != nil {
			u.log.Error("Failed to auto-create space room", zap.Error(err))
		} else {
			u.User, err = u.Update().SetSpaceRoom(resp.RoomID).Save(context.Background())
			if err != nil {
				u.log.Error("Failed to set user space room", zap.Error(err))
			}
			u.ensureInvited(u.bridge.Bot, u.SpaceRoom, false)
		}
	}

	return u.SpaceRoom
}

func (u *User) SetManagementRoom(roomID id.RoomID) {
	existingUser, ok := u.bridge.managementRooms[roomID]
	if ok {
		existingUser.ManagementRoom = ""
		_, err := existingUser.Update().SetManagementRoom("").Save(context.Background())
		u.log.Error("update management room fail",
			zap.String("matrix_id", string(u.MatrixID)),
			zap.String("management_room_id", string(roomID)),
			zap.Error(err))
	}

	u.ManagementRoom = roomID
	u.bridge.managementRooms[u.ManagementRoom] = u
	_, err := u.Update().SetManagementRoom(roomID).Save(context.Background())
	u.log.Error("update management room fail",
		zap.String("matrix_id", string(u.MatrixID)),
		zap.String("management_room_id", string(roomID)),
		zap.Error(err))
}

func (u *User) SetQQAndToken(ctx context.Context) {
	newUser, err := u.Update().SetQQ(u.client.Uin).SetQQToken(u.client.GenToken()).Save(ctx)

	if err != nil {
		u.log.Error("update qq and qq_token fail",
			zap.Error(err))
	} else {
		u.User = newUser
	}
}

func (u *User) Login(ctx context.Context, uid int64, password string) (*client.LoginResponse, error) {
	if err := u.NewEmptyClient(ctx); err != nil {
		return nil, err
	}

	u.client.Uin = uid
	u.client.PasswordMd5 = md5.Sum([]byte(password))

	return u.client.Login()
}

func (u *User) NewEmptyClient(ctx context.Context) error {
	if u.client != nil {
		if u.client.Online.Load() {
			return client.ErrAlreadyOnline
		}

		u.client.Release()
		u.client.Disconnect()
	}

	var err error
	u.client, err = qq.NewClient()
	if err != nil {
		return err
	}

	err = u.newDevice(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (u *User) FetchQRCode() (*client.QRCodeLoginResponse, error) {
	return u.client.FetchQRCode()
}

func (u *User) QueryQRCodeStatus(sig []byte) (*client.QRCodeLoginResponse, error) {
	return u.client.QueryQRCodeStatus(sig)
}

func (u *User) LoginQr(info *client.QRCodeLoginInfo) (*client.LoginResponse, error) {
	return u.client.QRCodeLogin(info)
}

func (u *User) LoginWithToken(ctx context.Context) error {
	var err error
	u.client, err = qq.NewClient()
	if err != nil {
		return err
	}

	err = u.newDevice(ctx)
	if err != nil {
		return err
	}

	u.client.Uin = u.QQ

	return u.client.TokenLogin(u.QQToken)
}

func (u *User) newDevice(ctx context.Context) error {
	deviceInfo := &client.DeviceInfo{}

	saveDeviceInfo := false
	if len(u.DeviceInfo) > 0 {
		if err := client.SystemDeviceInfo.ReadJson(u.DeviceInfo); err != nil {
			u.log.Warn("unmarshal device info fail, regenerate new device info", zap.Error(err))
			deviceInfo = qq.GenRandomDevice()
			saveDeviceInfo = true
		} else {
			deviceInfo = client.SystemDeviceInfo
		}
	} else {
		u.log.Info("device info not found, generate new device info")
		saveDeviceInfo = true
		deviceInfo = qq.GenRandomDevice()
	}

	u.client.UseDevice(deviceInfo)

	if saveDeviceInfo {
		u.DeviceInfo = deviceInfo.ToJson()
		var err error
		u.User, err = u.Update().SetDeviceInfo(deviceInfo.ToJson()).Save(ctx)
		return err
	}

	return nil
}

func (b *Bridge) NewUser(dbUser *ent.User) *User {
	return &User{
		User:   dbUser,
		bridge: b,
		log:    b.log.Named("user"),
	}
}

func (u *User) Connect() {
	u.log.Info("Initializing Client event handler")
	u.clientHandler = u.NewClientHandler(u, u.log.Named("clientHandler"))

	u.log.Info("Starting refresh friend list")
	err := u.client.ReloadFriendList()
	if err != nil {
		u.log.Error("Failed to refresh friend list", zap.Error(err))
	}

	u.log.Info("Starting refresh group list")
	err = u.client.ReloadGroupList()
	if err != nil {
		u.log.Error("Failed to refresh group list", zap.Error(err))
	}

	u.log.Info("Syncing group list to matrix")
	u.syncGroupList()
}

func (u *User) syncGroupList() {
	for _, info := range u.client.GroupList {
		p := u.bridge.GetPortalByGroupCode(info.Code)
		if p == nil {
			u.log.Warn("get portal by group code return nil", zap.Int64("qq_group_code", info.Code))
			continue
		}
		if len(p.MatrixID) == 0 {
			p.log.Debug("Creating Matrix room", zap.Int64("qq_group_code", info.Code))
			err := p.CreateMatrixRoom(u, info)
			if err != nil {
				p.log.Error("Failed to create portal room", zap.Error(err))
			}
		} else {
			p.UpdateMatrixRoom(u, info)
		}
	}
}

func (u *User) ensureInvited(intent *appservice.IntentAPI, roomID id.RoomID, isDirect bool) (ok bool) {
	inviteContent := event.Content{
		Parsed: &event.MemberEventContent{
			Membership: event.MembershipInvite,
			IsDirect:   isDirect,
		},
		Raw: map[string]interface{}{},
	}

	_, err := intent.SendStateEvent(roomID, event.StateMember, u.MatrixID.String(), &inviteContent)
	if err != nil && !strings.Contains(err.Error(), "is already in the room") {
		u.log.Error("Failed to SendStateEvent", zap.Error(err))
	} else {
		ok = true
	}

	return
}

type ClientHandler struct {
	user *User

	log *zap.Logger
}

func (u *User) NewClientHandler(user *User, log *zap.Logger) *ClientHandler {
	handler := &ClientHandler{user: user, log: log}

	handler.user.client.PrivateMessageEvent.Subscribe(handler.HandlePrivateMessage)
	handler.user.client.SelfPrivateMessageEvent.Subscribe(handler.HandlePrivateMessage)

	handler.user.client.GroupMessageEvent.Subscribe(handler.HandleGroupMessage)
	handler.user.client.SelfGroupMessageEvent.Subscribe(handler.HandleGroupMessage)

	return handler
}

func (handler *ClientHandler) HandlePrivateMessage(c *client.QQClient, msg *message.PrivateMessage) {

}

func (handler *ClientHandler) HandleGroupMessage(c *client.QQClient, msg *message.GroupMessage) {
	p := handler.user.bridge.GetPortalByGroupCode(msg.GroupCode)
	p.messages <- &PortalMessage{Message: msg, Source: handler.user}
}
