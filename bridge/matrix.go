package bridge

import (
	"go.uber.org/zap"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
	"strings"
	"sync"
)

type MatrixHandler struct {
	bridge     *Bridge
	appService *appservice.AppService
	cmd        *CommandHandler

	log *zap.Logger

	cmdSession      map[id.RoomID]chan *event.Event
	cmdSessionMutex sync.RWMutex
}

func NewMatrixHandler(bridge *Bridge, log *zap.Logger) *MatrixHandler {
	handler := &MatrixHandler{
		bridge:     bridge,
		appService: bridge.AppService,
		cmd:        NewCommandHandler(bridge),
		log:        log,
		cmdSession: map[id.RoomID]chan *event.Event{},
	}

	bridge.EventProcessor.On(event.EventMessage, handler.HandleMessage)
	bridge.EventProcessor.On(event.EventEncrypted, handler.HandleEncrypted)
	bridge.EventProcessor.On(event.EventSticker, handler.HandleMessage)
	bridge.EventProcessor.On(event.EventReaction, handler.HandleReaction)
	bridge.EventProcessor.On(event.EventRedaction, handler.HandleRedaction)
	bridge.EventProcessor.On(event.StateMember, handler.HandleMembership)
	bridge.EventProcessor.On(event.StateRoomName, handler.HandleRoomMetadata)
	bridge.EventProcessor.On(event.StateRoomAvatar, handler.HandleRoomMetadata)
	bridge.EventProcessor.On(event.StateTopic, handler.HandleRoomMetadata)
	bridge.EventProcessor.On(event.StateEncryption, handler.HandleEncryption)
	bridge.EventProcessor.On(event.EphemeralEventPresence, handler.HandlePresence)
	bridge.EventProcessor.On(event.EphemeralEventReceipt, handler.HandleReceipt)
	bridge.EventProcessor.On(event.EphemeralEventTyping, handler.HandleTyping)
	return handler
}

func (mx *MatrixHandler) shouldIgnoreEvent(evt *event.Event) bool {
	if _, isPuppet := mx.bridge.ParsePuppetMXID(evt.Sender); evt.Sender == mx.bridge.Bot.UserID || isPuppet {
		return true
	}

	return evt.Sender == mx.bridge.Bot.UserID
}

func (mx *MatrixHandler) HandleMessage(evt *event.Event) {
	if mx.shouldIgnoreEvent(evt) {
		return
	}

	user := mx.bridge.GetUserByMXID(evt.Sender)
	if user == nil {
		return
	}

	content := evt.Content.AsMessage()
	content.RemoveReplyFallback()
	if content.MsgType == event.MsgText {
		if c, ok := mx.cmdSession[evt.RoomID]; ok {
			c <- evt
			return
		}

		commandPrefix := mx.bridge.config.Bridge.CommandPrefix
		hasCommandPrefix := strings.HasPrefix(content.Body, commandPrefix)
		if hasCommandPrefix {
			content.Body = strings.TrimLeft(content.Body[len(commandPrefix):], " ")
		}

		if hasCommandPrefix || evt.RoomID == user.ManagementRoom {
			mx.cmd.Handle(evt.RoomID, evt.ID, user, content.Body, content.GetReplyTo())
			return
		}
	}

	portal := mx.bridge.GetPortalByMXID(evt.RoomID)
	if portal != nil {
		portal.matrixMessages <- &PortalMatrixMessage{Evt: evt, Source: user}
	}
}

func (mx *MatrixHandler) HandleEncrypted(evt *event.Event) {
}

func (mx *MatrixHandler) HandleReaction(evt *event.Event) {
}

func (mx *MatrixHandler) HandleRedaction(evt *event.Event) {
}

func (mx *MatrixHandler) HandleMembership(evt *event.Event) {
	if _, isPuppet := mx.bridge.ParsePuppetMXID(evt.Sender); evt.Sender == mx.bridge.Bot.UserID || isPuppet {
		return
	}

	content := evt.Content.AsMember()
	if content.Membership == event.MembershipInvite && id.UserID(evt.GetStateKey()) == mx.appService.BotMXID() {
		mx.HandleBotInvite(evt)
		return
	}

	mx.log.Debug("matrix handle membership type not support for now", zap.String("type", string(content.Membership)))
}

func (mx *MatrixHandler) HandleRoomMetadata(evt *event.Event) {
}

func (mx *MatrixHandler) HandleEncryption(evt *event.Event) {
}

func (mx *MatrixHandler) HandlePresence(evt *event.Event) {
}

func (mx *MatrixHandler) HandleReceipt(evt *event.Event) {
}

func (mx *MatrixHandler) HandleTyping(evt *event.Event) {
}

func (mx *MatrixHandler) HandleBotInvite(evt *event.Event) {
	intent := mx.appService.BotIntent()

	user := mx.bridge.GetUserByMXID(evt.Sender)
	if user == nil {
		return
	}

	members := mx.joinAndCheckMembers(evt, intent)
	if members == nil {
		return
	}

	mx.sendNoticeWithMarkdown(evt.RoomID, mx.bridge.config.Bridge.ManagementRoomText.Welcome)

	if len(members.Joined) == 2 && (len(user.ManagementRoom) == 0 || evt.Content.AsMember().IsDirect) {
		user.SetManagementRoom(evt.RoomID)
		_, _ = intent.SendNotice(user.ManagementRoom, "This room has been registered as your bridge management/status room.")
		mx.log.Sugar().Debug(evt.RoomID, "registered as a management room with", evt.Sender)
	}

	if evt.RoomID == user.ManagementRoom {
		mx.sendNoticeWithMarkdown(evt.RoomID, mx.bridge.config.Bridge.ManagementRoomText.WelcomeUnconnected)

		additionalHelp := mx.bridge.config.Bridge.ManagementRoomText.AdditionalHelp
		if len(additionalHelp) > 0 {
			mx.sendNoticeWithMarkdown(evt.RoomID, additionalHelp)
		}
	}
}

func (mx *MatrixHandler) joinAndCheckMembers(evt *event.Event, intent *appservice.IntentAPI) *mautrix.RespJoinedMembers {
	log := mx.log.Sugar()

	resp, err := intent.JoinRoomByID(evt.RoomID)
	if err != nil {
		log.Debugf("Failed to join room %s as %s with invite from %s: %v", evt.RoomID, intent.UserID, evt.Sender, err)
		return nil
	}

	members, err := intent.JoinedMembers(resp.RoomID)
	if err != nil {
		log.Debugf("Failed to get members in room %s after accepting invite from %s as %s: %v", resp.RoomID, evt.Sender, intent.UserID, err)
		intent.LeaveRoom(resp.RoomID)
		return nil
	}

	if len(members.Joined) < 2 {
		log.Debugf("Leaving empty room", resp.RoomID, "after accepting invite from", evt.Sender, "as", intent.UserID)
		intent.LeaveRoom(resp.RoomID)
		return nil
	}
	return members
}

func (mx *MatrixHandler) sendNoticeWithMarkdown(roomID id.RoomID, message string) (*mautrix.RespSendEvent, error) {
	intent := mx.appService.BotIntent()
	content := format.RenderMarkdown(message, true, false)
	content.MsgType = event.MsgNotice
	return intent.SendMessageEvent(roomID, event.EventMessage, content)
}
