package bridge

import (
	"context"
	"fmt"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/puppet"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/schema"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/qq"
	"go.uber.org/zap"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/id"
	"regexp"
	"strconv"
)

type Puppet struct {
	*ent.Puppet

	log *zap.Logger

	bridge *Bridge

	MatrixID id.UserID

	customIntent *appservice.IntentAPI
}

var userIDRegex *regexp.Regexp

// ParsePuppetMXID Get puppet uid by matrix id
func (b *Bridge) ParsePuppetMXID(mxID id.UserID) (uid int64, ok bool) {
	if userIDRegex == nil {
		userIDRegex = regexp.MustCompile(fmt.Sprintf("^@%s:%s$",
			b.config.Bridge.FormatUsername("([0-9]+)"),
			b.config.Homeserver.Domain))
	}
	match := userIDRegex.FindStringSubmatch(string(mxID))
	if len(match) == 2 {
		if i, err := strconv.ParseInt(match[1], 10, 64); err == nil {
			uid = i
			ok = true
		}
	}
	return
}

func (b *Bridge) FormatPuppetMXID(uin int64) id.UserID {
	return id.NewUserID(
		b.config.Bridge.FormatUsername(strconv.FormatInt(uin, 10)),
		b.config.Homeserver.Domain)
}

func (b *Bridge) NewPuppet(dbPuppet *ent.Puppet) *Puppet {
	return &Puppet{
		Puppet: dbPuppet,
		bridge: b,
		log:    b.log.Named(fmt.Sprintf("Puppet/%d", dbPuppet.Uin)),

		MatrixID: b.FormatPuppetMXID(dbPuppet.Uin),
	}
}

func (b *Bridge) GetPuppetByUin(uin int64) *Puppet {
	ctx := context.Background()

	b.puppetsLock.Lock()
	defer b.puppetsLock.Unlock()
	p, ok := b.puppets[uin]
	if !ok {
		dbPuppet, err := b.db.Puppet.Query().Where(puppet.Uin(uin)).Only(ctx)
		if ent.IsNotFound(err) {
			dbPuppet, err = b.db.Puppet.Create().SetUin(uin).Save(ctx)
			if err != nil {
				b.log.Error(err.Error())
				dbPuppet.Uin = uin
			}
		} else if err != nil {
			b.log.Error(err.Error())
			dbPuppet.Uin = uin
		}

		p = b.NewPuppet(dbPuppet)
		b.puppets[uin] = p
		if len(p.CustomMatrixID) > 0 {
			b.puppetsByCustomMXID[p.CustomMatrixID] = p
		}
	}
	return p
}

func (p *Puppet) IntentFor(portal *Portal) *appservice.IntentAPI {
	if p.customIntent == nil || portal.Type != schema.GroupPortalType {
		return p.DefaultIntent()
	}

	return p.customIntent
}

func (p *Puppet) DefaultIntent() *appservice.IntentAPI {
	return p.bridge.AppService.Intent(p.MatrixID)
}

func (p *Puppet) UpdateDisplayName(ctx context.Context, displayName string) error {
	if displayName != "" && p.DisplayName != displayName {
		err := p.DefaultIntent().SetDisplayName(p.bridge.config.Bridge.FormatDisplayName(displayName))
		if err != nil {
			return err
		}

		if tmp, err := p.Update().SetDisplayName(displayName).Save(ctx); err != nil {
			return err
		} else {
			p.Puppet = tmp
		}
	}

	return nil
}

func (p *Puppet) UpdateAvatar(ctx context.Context, avatarURL string) (err error) {
	mediaUpload, err := p.DefaultIntent().UploadLink(avatarURL)
	if err != nil {
		return
	}

	err = p.DefaultIntent().SetAvatarURL(mediaUpload.ContentURI)
	if err != nil {
		return
	}

	tmp, err := p.Update().SetAvatarURL(avatarURL).Save(ctx)
	if err == nil {
		p.Puppet = tmp
	}
	return
}

func (p *Puppet) Sync(ctx context.Context, displayName string) error {
	err := p.DefaultIntent().EnsureRegistered()
	if err != nil {
		return err
	}

	if err = p.UpdateDisplayName(ctx, displayName); err != nil {
		return err
	}

	url, err := qq.GetAvatarURL(p.Uin)
	if err != nil {
		return err
	}

	if len(p.AvatarURL) == 0 || p.AvatarURL != url {
		err := p.UpdateAvatar(ctx, url)
		if err != nil {
			p.log.Error("update puppet avatar failed", zap.Error(err))
		}
	}

	return nil
}
