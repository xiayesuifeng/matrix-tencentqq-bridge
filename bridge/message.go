package bridge

import (
	"bytes"
	"context"
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqljson"
	"fmt"
	"github.com/Mrs4s/MiraiGo/message"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
	dbMsg "gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent/message"
	"maunium.net/go/mautrix/appservice"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
	"time"
)

func (b *Bridge) GetMessageByMsgID(ctx context.Context, msgID int32, portalMatrixID id.RoomID) (*ent.Message, error) {
	return b.db.Message.Query().Where(dbMsg.MsgID(msgID), dbMsg.PortalMatrixID(portalMatrixID)).Only(ctx)
}

func (b *Bridge) GetMessageByEventID(ctx context.Context, evtID id.EventID) (*ent.Message, error) {
	return b.db.Message.Query().
		Where(func(selector *sql.Selector) { selector.Where(sqljson.ValueContains(dbMsg.FieldEventID, evtID)) }).
		Only(ctx)
}

func (b *Bridge) CreateMessage(ctx context.Context, msgID int32, portalMatrixID id.RoomID, eventIDs []id.EventID) (*ent.Message, error) {
	return b.db.Message.Create().SetMsgID(msgID).SetPortalMatrixID(portalMatrixID).SetEventID(eventIDs).Save(ctx)
}

type PortalMessage struct {
	Message interface{}
	Source  *User
}

type ConvertMessage struct {
	PrimaryContent event.MessageEventContent
	ExtraContent   []*event.MessageEventContent
	RelatesTo      id.EventID
}

func (p *PortalMessage) UnixMilliTime() int64 {
	var time32 int32
	switch msg := p.Message.(type) {
	case *message.PrivateMessage:
		time32 = msg.Time
	case *message.GroupMessage:
		time32 = msg.Time
	}

	if time32 == 0 {
		return time.Now().UnixMilli()
	} else {
		return time.Unix(int64(time32), 0).UnixMilli()
	}
}

func (p *PortalMessage) convertMatrixMessage(ctx context.Context, roomID id.RoomID) *ConvertMessage {
	convertMsg := &ConvertMessage{
		ExtraContent: make([]*event.MessageEventContent, 0),
	}

	formatText := ""

	notSupportType := ""

	skipReplyAt := false

	switch msg := p.Message.(type) {
	case *message.PrivateMessage:
	case *message.GroupMessage:
		for _, element := range msg.Elements {
			switch e := element.(type) {
			case *message.TextElement:
				formatText += e.Content
			case *message.FaceElement:
				formatText += "[" + e.Name + "]"
			case *message.MarketFaceElement:
				formatText += "[" + e.Name + "]"
			case *message.GroupImageElement:
				if e.Flash {
					notSupportType += "[闪照]"
				} else {
					convertMsg.ExtraContent = append(convertMsg.ExtraContent, &event.MessageEventContent{
						MsgType: event.MsgImage,
						Body:    e.ImageId,
						URL:     id.ContentURIString(e.Url),
					})
				}
			case *message.AtElement:
				if skipReplyAt {
					skipReplyAt = false
					continue
				}

				formatText += fmt.Sprintf("<a href=\"https://matrix.to/#/%s\">%s</a>:",
					p.Source.bridge.FormatPuppetMXID(e.Target),
					p.Source.bridge.config.Bridge.FormatDisplayName(e.Display))
			case *message.RedBagElement:
				formatText += "[RedBag:" + e.Title + "]"
			case *message.ServiceElement:
				formatText += "[Service:" + e.Content + "]"
			case *message.LightAppElement:
				formatText += "[LightApp:" + e.Content + "]"
			case *message.ReplyElement:
				m, err := p.Source.bridge.GetMessageByMsgID(ctx, e.ReplySeq, roomID)
				if err == nil && len(m.EventID) > 0 {
					skipReplyAt = true
					convertMsg.RelatesTo = m.EventID[0]
				}
			case *message.GroupFileElement:
				convertMsg.ExtraContent = append(convertMsg.ExtraContent, &event.MessageEventContent{
					MsgType: event.MsgFile,
					Body:    e.Name,
					URL:     id.ContentURIString(p.Source.client.GetGroupFileUrl(msg.GroupCode, e.Path, e.Busid)),
				})
			default:
				notSupportType += "[" + e.Type().String() + "]"
			}
		}
	}

	if len(formatText) == 0 && len(notSupportType) == 0 && len(convertMsg.ExtraContent) == 0 {
		formatText = "不支持的消息类型" + notSupportType
	}

	if len(formatText) == 0 && len(convertMsg.ExtraContent) > 0 {
		convertMsg.PrimaryContent = *convertMsg.ExtraContent[0]
		convertMsg.ExtraContent = convertMsg.ExtraContent[1:]
	} else {
		convertMsg.PrimaryContent = format.RenderMarkdown(formatText, true, true)
	}

	return convertMsg
}

type PortalMatrixMessage struct {
	Evt    *event.Event
	Source *User
}

func (p *PortalMatrixMessage) convertMatrixMessage(ctx context.Context, intentAPI *appservice.IntentAPI, roomID id.RoomID, isGroup bool, qqID int64) (elements []message.IMessageElement, err error) {
	msg := p.Evt.Content.AsMessage()

	replyToID := msg.GetReplyTo()
	if len(replyToID) > 0 {
		qMsg, err := p.Source.bridge.GetMessageByEventID(ctx, replyToID)
		if err != nil {
			return nil, err
		}

		evt, err := intentAPI.GetEvent(roomID, replyToID)
		if err != nil {
			return nil, err
		}

		uin, ok := p.Source.bridge.ParsePuppetMXID(evt.Sender)

		if err == nil && ok {
			elements = append(elements,
				&message.ReplyElement{
					ReplySeq: qMsg.MsgID,
					Sender:   uin,
					Time:     int32(evt.Timestamp / 1e3),
					GroupID:  p.Source.bridge.GetPortalByMXID(p.Evt.RoomID).QqID,
					Elements: []message.IMessageElement{message.NewText("")},
				},
				message.NewAt(uin))
		}
	}

	if msg.MsgType == event.MsgText || msg.MsgType == event.MsgNotice || msg.MsgType == event.MsgEmote {
		elements = append(elements, message.NewText(msg.Body))
	} else if msg.MsgType == event.MsgImage {
		source := message.Source{
			SourceType: message.SourcePrivate,
			PrimaryID:  qqID,
		}

		if isGroup {
			source.SourceType = message.SourceGroup
		}

		var url id.ContentURI
		if msg.File != nil {
			url, err = msg.File.URL.Parse()
		} else {
			url, err = msg.URL.Parse()
		}
		if err != nil {
			return
		}

		imgBytes, err := intentAPI.DownloadBytes(url)
		if err != nil {
			return nil, err
		}

		imgElement, err := p.Source.client.UploadImage(source, bytes.NewReader(imgBytes))
		if err != nil {
			return nil, err
		}

		elements = append(elements, imgElement)
	}

	return
}
