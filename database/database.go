package database

import (
	"context"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/config"
	"gitlab.com/xiayesuifeng/matrix-tencentqq-bridge/ent"
)

func NewClient(config config.DatabaseConfig) (*ent.Client, error) {
	client, err := ent.Open(config.Type, config.URI)
	if err != nil {
		return nil, err
	}

	if err := client.Schema.Create(context.Background()); err != nil {
		return nil, err
	}

	return client, nil
}
