module gitlab.com/xiayesuifeng/matrix-tencentqq-bridge

go 1.18

require (
	entgo.io/ent v0.10.1
	github.com/Mrs4s/MiraiGo v0.0.0-20220405144558-6e7053381e86
	github.com/lib/pq v1.10.4
	github.com/mattn/go-sqlite3 v1.14.12
	go.uber.org/zap v1.21.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	maunium.net/go/mautrix v0.10.12
)

require (
	ariga.io/atlas v0.3.7-0.20220303204946-787354f533c3 // indirect
	github.com/RomiChan/protobuf v0.0.0-20220318113238-d8a99598f896 // indirect
	github.com/RomiChan/syncx v0.0.0-20220320130821-c88644afda9c // indirect
	github.com/agext/levenshtein v1.2.1 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/fumiama/imgsz v0.0.2 // indirect
	github.com/go-openapi/inflect v0.19.0 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/hcl/v2 v2.10.0 // indirect
	github.com/mitchellh/go-wordwrap v0.0.0-20150314170334-ad45545899c7 // indirect
	github.com/pierrec/lz4/v4 v4.1.11 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/tidwall/gjson v1.14.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/zclconf/go-cty v1.8.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/crypto v0.0.0-20220315160706-3147a52a75dd // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	maunium.net/go/maulogger/v2 v2.3.2 // indirect
)
