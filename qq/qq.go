package qq

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/Mrs4s/MiraiGo/client"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
)

type Client struct {
	*client.QQClient
}

func NewClient() (*Client, error) {
	c := &Client{
		QQClient: client.NewClientEmpty(),
	}

	return c, nil
}

// GenRandomDevice 生成随机设备信息
func GenRandomDevice() *client.DeviceInfo {
	client.GenRandomDevice()

	client.SystemDeviceInfo.Protocol = client.IPad

	return client.SystemDeviceInfo
}

// GetAvatarURL Get QQ avatar url by uin
func GetAvatarURL(uin int64) (string, error) {
	resp, err := http.Get(fmt.Sprintf("https://ptlogin2.qq.com/getface?&imgtype=3&uin=%d", uin))
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", errors.New(string(body))
	}

	reg, err := regexp.Compile("pt.setHeader\\({\"\\d+\"*:*\"(.*)\"}\\)")
	if err != nil {
		return "", err
	}

	result := reg.FindStringSubmatch(string(body))
	if len(result) < 2 {
		return "", errors.New("get avatarUrl failed")
	}

	return result[1], nil
}

// GetGroupAvatarURL Get QQ group avatar url by groupCode
func GetGroupAvatarURL(groupCode int64) string {
	return fmt.Sprintf("https://p.qlogo.cn/gh/%d/%d/0", groupCode, groupCode)
}

// GetGroupAvatar Get QQ group avatar bytes and md5 by groupCode
func GetGroupAvatar(groupCode int64) ([]byte, string, error) {
	url := GetGroupAvatarURL(groupCode)

	resp, err := http.Get(url)
	if err != nil {
		return nil, "", err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, "", errors.New("get url response status code not 200")
	}

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, "", err
	}

	hash := md5.New()
	hash.Write(bytes)

	return bytes, hex.EncodeToString(hash.Sum(nil)), nil
}
